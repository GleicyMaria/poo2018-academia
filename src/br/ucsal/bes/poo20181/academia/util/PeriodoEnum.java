package br.ucsal.bes.poo20181.academia.util;

public enum PeriodoEnum {
	
	MANHA("Manha"), TARDE("Tarde"), NOITE("Noite");

	private String Periodo;

	private PeriodoEnum(String Periodo) {
		this.Periodo = Periodo;
	}

	public String getPeriodo() {
		return Periodo;
	}

}
