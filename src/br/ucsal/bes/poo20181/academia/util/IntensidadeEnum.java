package br.ucsal.bes.poo20181.academia.util;

public enum IntensidadeEnum {

	LEVE("Leve"), MEDIO("Medio"), PESADO("Pesado");

	private String intensidade;

	private IntensidadeEnum(String intensidade) {
		this.intensidade = intensidade;
	}

	public String getIntensidade() {
		return intensidade;
	}
	
}
