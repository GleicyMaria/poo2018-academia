package br.ucsal.bes.poo20181.academia.util;

public enum ModalidadeEnum {
	MUSCULACAO("Muscula��o"), DANCA("Dan�a"), NATACAO("Nata��o");

	private String modalidade;

	private ModalidadeEnum(String modalidade) {
		this.modalidade = modalidade;
	}

	public String getModalidade() {
		return modalidade;
	}
}
