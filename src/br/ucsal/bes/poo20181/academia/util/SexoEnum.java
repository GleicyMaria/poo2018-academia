package br.ucsal.bes.poo20181.academia.util;

public enum SexoEnum {
	MASCULINO("Masculino"), FEMININO("Feminino");

	private String sexo;

	SexoEnum(String sexo) {
		this.sexo = sexo;
	}

	public String getSexo() {
		return sexo;
	}
}
