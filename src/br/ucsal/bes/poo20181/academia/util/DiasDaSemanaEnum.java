package br.ucsal.bes.poo20181.academia.util;

public enum DiasDaSemanaEnum {
	
	SEGUNDA("Segunda"), TERCA ("Ter�a"), QUARTA ("Quarta"), QUINTA ("Quinta"), SEXTA ("Sexta");

		private String DiasDaSemana;

		private DiasDaSemanaEnum(String DiasDaSemana) {
			this.DiasDaSemana = DiasDaSemana;
		}

		public String getDiasDaSemana() {
			return DiasDaSemana;
		}
	

}
