package br.ucsal.bes.poo20181.academia.controller;

import java.util.Date;

import br.ucsal.bes.poo20181.academia.domain.Endereco;
import br.ucsal.bes.poo20181.academia.domain.Pessoa;
import br.ucsal.bes.poo20181.academia.domain.Professor;
import br.ucsal.bes.poo20181.academia.model.Academia;
import br.ucsal.bes.poo20181.academia.util.ModalidadeEnum;
import br.ucsal.bes.poo20181.academia.util.PerfilEnum;

public class ProfessorController {

	private static Endereco endereco;

	public static void cadastrarEndereco(String rua, String numero, String bairro, String cidade, String cep) {
		endereco = new Endereco(bairro, rua, Integer.parseInt(numero), cidade, cep);
	}

	public static void cadastrarDados(String nome, Date dataNascimento, String formacao, ModalidadeEnum modalidade) {
		Academia.usuarios.add(new Professor(nome, dataNascimento, endereco, formacao, modalidade));
	}

	public static Pessoa buscar(String cpf) {
		for (Pessoa usuario : Academia.usuarios) {
			if (usuario.getCpf().equals(cpf)) {
				return usuario;
			}
		}
		return null;
	}

	public static boolean remover(Professor professor) {
		if (professor != null) {
			int pos = Academia.usuarios.indexOf(professor);
			if (pos > -1) {
				Academia.usuarios.remove(professor);
				return true;
			}
		}
		return false;
	}

	public static void alterarDados(Professor usuario){
		usuario.setPerfil(PerfilEnum.PROFESSOR);
        for (int i = 0; i < Academia.usuarios.size(); i++) {
            if (Academia.usuarios.get(i).getMatricula() != null && usuario.getMatricula() == usuario.getMatricula()){
                Academia.usuarios.set(i, usuario);
            }
        }
    }
}
