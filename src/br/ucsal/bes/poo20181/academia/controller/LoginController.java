package br.ucsal.bes.poo20181.academia.controller;

import br.ucsal.bes.poo20181.academia.domain.Pessoa;
import br.ucsal.bes.poo20181.academia.model.Academia;

public class LoginController {
    public static Pessoa validarSenha(String login, String senha) {
        for (Pessoa usuario : Academia.usuarios) {
            if (usuario.getConta().getLogin().equalsIgnoreCase(login)
                    && usuario.getConta().getSenha().equalsIgnoreCase(senha)) {
                return usuario;
            }
        }
        return null;
    }

}
