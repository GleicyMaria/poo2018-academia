package br.ucsal.bes.poo20181.academia.controller;

import java.util.Date;

import br.ucsal.bes.poo20181.academia.domain.Cliente;
import br.ucsal.bes.poo20181.academia.domain.Endereco;
import br.ucsal.bes.poo20181.academia.domain.Pessoa;
import br.ucsal.bes.poo20181.academia.model.Academia;
import br.ucsal.bes.poo20181.academia.util.PerfilEnum;
import br.ucsal.bes.poo20181.academia.util.SexoEnum;

public class AlunoController {

    private static Endereco endereco;

    public static void cadastrarEndereco(String rua, String numero, String bairro, String cidade, String cep) {
        endereco = new Endereco(bairro, rua, Integer.parseInt(numero), cidade, cep);
    }

    public static void cadastrarDados(String nome, String cpf, Date dataNascimento, String email, SexoEnum sexo) {
        Academia.usuarios.add(new Cliente(nome, cpf, dataNascimento, endereco, email, sexo));
    }

    public static Pessoa buscar(String cpf) {
        for (Pessoa alunos : Academia.usuarios) {
            if (alunos.getCpf().equals(cpf)) {
                return alunos;
            }
        }
        return null;
    }

    public static void alterarDados(Cliente usuario){
        usuario.setPerfil(PerfilEnum.ALUNO);
        for (int i = 0; i < Academia.usuarios.size(); i++) {
            if (Academia.usuarios.get(i).getMatricula() != null && usuario.getMatricula() == usuario.getMatricula()){
                Academia.usuarios.set(i, usuario);
            }
        }
    }

}
