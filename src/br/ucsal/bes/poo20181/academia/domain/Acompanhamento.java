package br.ucsal.bes.poo20181.academia.domain;

import java.util.ArrayList;
import java.util.List;

public class Acompanhamento {

	private List<Turma> turmas = new ArrayList<>();
	private List<Cliente> alunos = new ArrayList<>();
	private List<Professor> professores = new ArrayList<>();

	public Acompanhamento(List<Turma> turmas) {
		this.turmas = turmas;
	}

	public List<Turma> getTurmas() {
		return turmas;
	}

	public List<Cliente> getAlunos() {
		return alunos;
	}

	public List<Professor> getProfessores() {
		return professores;
	}

}
