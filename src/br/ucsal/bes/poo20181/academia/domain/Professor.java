package br.ucsal.bes.poo20181.academia.domain;

import java.util.Date;

import br.ucsal.bes.poo20181.academia.util.ModalidadeEnum;
import br.ucsal.bes.poo20181.academia.util.PerfilEnum;

public class Professor extends Pessoa {

	private static Integer next = 0;

	private ModalidadeEnum modalidade;
	private String formacao;
	private boolean ativo;
	private int matricula;

	public Professor(String nome, Date dataNascimento, Endereco endereco, String formacao, ModalidadeEnum modalidade) {
		super(nome, dataNascimento, endereco);
		this.modalidade = modalidade;
		this.formacao = formacao;
		this.ativo = true;
		gerarMatricula();
		super.setPerfil(PerfilEnum.PROFESSOR);
	}

	public ModalidadeEnum getModalidade() {
		return modalidade;
	}

	public String getFormacao() {
		return formacao;
	}

	public void setFormacao(String formacao) {
		this.formacao = formacao;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public Integer getMatricula() {
		return matricula;
	}

	public void setMatricula(int matricula) {
		this.matricula = matricula;
	}

	private void gerarMatricula() {
		this.matricula = ++next;
	}
}
