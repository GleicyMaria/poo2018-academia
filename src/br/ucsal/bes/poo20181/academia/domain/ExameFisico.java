package br.ucsal.bes.poo20181.academia.domain;

import java.util.Date;

public class ExameFisico {

	private Date data;
	private CaracteristicaFisica caractFisic;

	public ExameFisico(Date data) {
		this.data = data;
		caractFisic = new CaracteristicaFisica();
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public CaracteristicaFisica getCaractFisic() {
		return caractFisic;
	}

	public void setCaractFisic(CaracteristicaFisica caractFisic) {
		this.caractFisic = caractFisic;
	}

	public double obterImc() {
		if (caractFisic != null)
			return (caractFisic.getPeso() / caractFisic.getAltura()) * caractFisic.getAltura();
		else
			return -1;
	}
}
