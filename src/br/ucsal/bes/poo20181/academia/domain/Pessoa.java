package br.ucsal.bes.poo20181.academia.domain;

import java.util.Date;

import br.ucsal.bes.poo20181.academia.util.PerfilEnum;
import br.ucsal.bes.poo20181.academia.util.SexoEnum;

public class Pessoa {
	

	private String nome;
	private Date dataNascimento;
	private Endereco endereco;
	private int telefone;
	private String email;
	private SexoEnum sexo;
	private Date dataCadastro;
	private String rg;
	private String cpf;
	private boolean comprovanteResid;

	private Conta conta;
	private PerfilEnum perfil;

	private Integer matricula = 0;
	private static Integer idMatricula = 0;
	
	

    public Pessoa() {
		this.dataCadastro = new Date();
		gerarMatricula();
	}

	public Pessoa(String nome, Date dataNascimento, Endereco endereco) {
		this();
		this.nome = nome;
		this.dataNascimento = dataNascimento;
		this.endereco = endereco;
		setConta(new Conta(nome.split(" ")[0], nome.split(" ")[0] + 123));
	}

	public Pessoa(String nome, Date dataNascimento, Endereco endereco, String email, SexoEnum sexo) {
		this(nome, dataNascimento, endereco);
		this.email = email;
		this.sexo = sexo;
		setConta(new Conta(nome.split(" ")[0], nome.split(" ")[0] + 123));
	}

	public void setConta(Conta conta) {
		this.conta = conta;
	}

	public Conta getConta() {
		return conta;
	}

	public Pessoa(String nome, Date dataNascimento, Endereco endereco, String email, int telefone, SexoEnum sexo,
			String rg, String cpf, boolean comprovanteResid) {
		this(nome, dataNascimento, endereco, email, sexo);
		this.telefone = telefone;
		this.rg = rg;
		this.cpf = cpf;
		this.comprovanteResid = comprovanteResid;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome){
	    this.nome = nome;
    }

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public PerfilEnum getPerfil() {
		return perfil;
	}

	public void setPerfil(PerfilEnum perfil) {
		this.perfil = perfil;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public int getTelefone() {
		return telefone;
	}

	public void setTelefone(int telefone) {
		this.telefone = telefone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public SexoEnum getSexo() {
		return sexo;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

    public Integer getMatricula() {
        return matricula;
    }

	public boolean isComprovanteResid() {
		return comprovanteResid;
	}

	public void setComprovanteResid(boolean comprovanteResid) {
		this.comprovanteResid = comprovanteResid;
	}
   
	
	private void gerarMatricula(){
        matricula = ++idMatricula;
    }
	
	
}
