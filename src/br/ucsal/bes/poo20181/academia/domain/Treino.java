package br.ucsal.bes.poo20181.academia.domain;

public class Treino {
	private String nome;
	private String descricao;
	private AtividadeFisica atividadeFisica;

	public Treino(String nome, String descricao, AtividadeFisica atividadesFisica) {
		this.nome = nome;
		this.descricao = descricao;
		this.atividadeFisica = atividadesFisica;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public AtividadeFisica getExercicio() {
		return atividadeFisica;
	}

	public void setExercicio(AtividadeFisica exercicio) {
		this.atividadeFisica = exercicio;
	}
}
