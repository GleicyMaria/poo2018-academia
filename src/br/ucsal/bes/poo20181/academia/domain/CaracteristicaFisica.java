package br.ucsal.bes.poo20181.academia.domain;

public class CaracteristicaFisica {

	private String tipoFisico;
	private double altura;
	private double peso;
	private double peito;
	private double cintura;
	private double pantuDireita;
	private double pantuEsquerda;
	private double coxaDireita;
	private double coxaEsquerda;
	private double bracoDireito;
	private double bracoEsquerdo;
	private double antebracoDireito;
	private double antebracoEsquerdo;
	private double gluteo;

	public String getTipoFisico() {
		return tipoFisico;
	}

	public void setTipoFisico(String tipoFisico) {
		this.tipoFisico = tipoFisico;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	public double getPeito() {
		return peito;
	}

	public void setPeito(double peito) {
		this.peito = peito;
	}

	public double getCintura() {
		return cintura;
	}

	public void setCintura(double cintura) {
		this.cintura = cintura;
	}

	public double getPantuDireita() {
		return pantuDireita;
	}

	public void setPantuDireita(double pantuDireita) {
		this.pantuDireita = pantuDireita;
	}

	public double getPantuEsquerda() {
		return pantuEsquerda;
	}

	public void setPantuEsquerda(double pantuEsquerda) {
		this.pantuEsquerda = pantuEsquerda;
	}

	public double getCoxaDireita() {
		return coxaDireita;
	}

	public void setCoxaDireita(double coxaDireita) {
		this.coxaDireita = coxaDireita;
	}

	public double getCoxaEsquerda() {
		return coxaEsquerda;
	}

	public void setCoxaEsquerda(double coxaEsquerda) {
		this.coxaEsquerda = coxaEsquerda;
	}

	public double getBracoDireito() {
		return bracoDireito;
	}

	public void setBracoDireito(double bracoDireito) {
		this.bracoDireito = bracoDireito;
	}

	public double getBracoEsquerdo() {
		return bracoEsquerdo;
	}

	public void setBracoEsquerdo(double bracoEsquerdo) {
		this.bracoEsquerdo = bracoEsquerdo;
	}

	public double getAntebracoDireito() {
		return antebracoDireito;
	}

	public void setAntebracoDireito(double antebracoDireito) {
		this.antebracoDireito = antebracoDireito;
	}

	public double getAntebracoEsquerdo() {
		return antebracoEsquerdo;
	}

	public void setAntebracoEsquerdo(double antebracoEsquerdo) {
		this.antebracoEsquerdo = antebracoEsquerdo;
	}

	public double getGluteo() {
		return gluteo;
	}

	public void setGluteo(double gluteo) {
		this.gluteo = gluteo;
	}

}
