package br.ucsal.bes.poo20181.academia.domain;

public class AtividadeFisica {
	private String nome;
	private int serie;
	private int repeticao;
	private String grupoMuscular;

	public AtividadeFisica(String nome, int serie, int repeticao, String grupoMuscular) {
		this.nome = nome;
		this.serie = serie;
		this.repeticao = repeticao;
		this.grupoMuscular = grupoMuscular;
	}

	public String getNome() {
		return nome;
	}

	public int getSerie() {
		return serie;
	}

	public void setSerie(int serie) {
		this.serie = serie;
	}

	public int getRepeticao() {
		return repeticao;
	}

	public void setRepeticao(int repeticao) {
		this.repeticao = repeticao;
	}

	public String getGrupoMuscular() {
		return grupoMuscular;
	}

	public void setGrupoMuscular(String grupoMuscular) {
		this.grupoMuscular = grupoMuscular;
	}
}
