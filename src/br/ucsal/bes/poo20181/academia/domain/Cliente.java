package br.ucsal.bes.poo20181.academia.domain;

import java.util.Date;

import br.ucsal.bes.poo20181.academia.util.PerfilEnum;
import br.ucsal.bes.poo20181.academia.util.SexoEnum;

public class Cliente extends Pessoa {
	private boolean ativo;

	public Cliente(String nome, String cpf, Date dataNascimento, Endereco endereco, String email, SexoEnum sexo) {
		super(nome, dataNascimento, endereco, email, sexo);
		super.setPerfil(PerfilEnum.ALUNO);
		super.setCpf(cpf);
	}

	public Cliente(String nome, Date dataNascimento, Endereco endereco) {
		super(nome, dataNascimento, endereco);
		super.setPerfil(PerfilEnum.ALUNO);
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}


}