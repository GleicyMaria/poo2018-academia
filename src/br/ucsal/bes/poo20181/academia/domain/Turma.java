package br.ucsal.bes.poo20181.academia.domain;

import java.util.ArrayList;
import java.util.List;

public class Turma {
	private static Integer cont = 0;

	private Integer numero;
	private Pessoa professor;
	private Treino treino;
	private List<Cliente> alunos;

	public Turma(Pessoa professor) {
		gerarNumero();
		this.professor = professor;
		this.alunos = new ArrayList<>();
	}

	public Turma(Pessoa professor, List<Cliente> alunos) {
		this(professor);
		this.alunos = alunos;
	}

	public Pessoa getProfessor() {
		return professor;
	}

	public List<Cliente> getAlunos() {
		return alunos;
	}

	public void adicionarAluno(Cliente aluno) {
		alunos.add(aluno);
	}

	public Integer getNumero() {
		return numero;
	}

	private void gerarNumero() {
		numero = ++cont;
	}

	public Treino getTreino() {
		return treino;
	}

}
