package br.ucsal.bes.poo20181.academia.gui.instrutor;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import br.ucsal.bes.poo20181.academia.model.Academia;
import br.ucsal.bes.poo20181.academia.util.DiasDaSemanaEnum;
import br.ucsal.bes.poo20181.academia.util.IntensidadeEnum;
import br.ucsal.bes.poo20181.academia.util.ModalidadeEnum;

public class PainelCadastAtividade extends JPanel {

	private static final long serialVersionUID = 1L;
	private JTextField campoNomeAtividade, campoQuantidade;
	private JButton cadastrar, limpar;

	public PainelCadastAtividade() {
		setBackground(new Color(255, 255, 255));
		setLayout(null);
		setPreferredSize(new Dimension(1000, 500));
		prepararcadastrarAtividade();
	}

	private void prepararcadastrarAtividade() {

		JPanel painelSuperior = new JPanel();
		painelSuperior.setBackground(new Color(51, 204, 255));
		painelSuperior.setBounds(0, 0, 574, 56);
		add(painelSuperior);
		painelSuperior.setLayout(null);

		JLabel cadastrarAtividade = new JLabel("Cadastrar Nova Atividade");
		cadastrarAtividade.setBounds(10, 11, 300, 34);
		cadastrarAtividade.setForeground(Color.WHITE);
		cadastrarAtividade.setFont(new Font("Dialog", Font.BOLD, 16));
		painelSuperior.add(cadastrarAtividade);

		adicionarLabels();
		adicionarCampo();
		adicionarComboBox();
		prepararBotoes();
	}

	private void prepararBotoes() {
		adicionarBotaoCadastrar();
		adicionarBotaoLimpar();
	}

	private void adicionarLabels() {
		JLabel txtAluno = new JLabel("Aluno:");
		txtAluno.setFont(new Font("Dialog", Font.BOLD, 12));
		txtAluno.setBounds(10, 104, 200, 14);
		add(txtAluno);

		JLabel txtDia = new JLabel(" Dia da Semana :  ");
		txtDia.setFont(new Font("Dialog", Font.BOLD, 12));
		txtDia.setBounds(10, 160, 250, 14);
		add(txtDia);
		
		JLabel txtModalidade = new JLabel("Modalidade:");
		txtModalidade.setFont(new Font("Dialog", Font.BOLD, 12));
		txtModalidade.setBounds(10, 216, 90, 14);
		add(txtModalidade);

		
		JLabel txtAtividade = new JLabel("Nome da Atividade :  ");
		txtAtividade.setFont(new Font("Dialog", Font.BOLD, 12));
		txtAtividade.setBounds(10, 272, 250, 14);
		add(txtAtividade);
		
		JLabel txtIntensidade = new JLabel("Intensidade :  ");
		txtIntensidade.setFont(new Font("Dialog", Font.BOLD, 12));
		txtIntensidade.setBounds(10, 328, 250, 14);
		add(txtIntensidade);
		
		JLabel txtQuantidade = new JLabel("Quantidade :  ");
		txtQuantidade.setFont(new Font("Dialog", Font.BOLD, 12));
		txtQuantidade.setBounds(10, 384, 250, 14);
		add(txtQuantidade);
			
		
	}

	private void adicionarCampo() {
		campoNomeAtividade = new JTextField();
		campoNomeAtividade.setBounds(20, 297, 231, 20);
		add(campoNomeAtividade);
		campoNomeAtividade.setColumns(10);
		
		
		campoQuantidade = new JTextField();
		campoQuantidade.setBounds(20, 409, 231, 20);
		add(campoQuantidade);
		campoQuantidade.setColumns(10);

	}

	private void adicionarComboBox() {

		JComboBox <String> comboBoxAluno = new JComboBox<String>();
		comboBoxAluno.setBounds(20, 129, 442, 20);
		add(comboBoxAluno);
		
		JComboBox<DiasDaSemanaEnum> diaDaSemana = new JComboBox<>(DiasDaSemanaEnum.values());
		diaDaSemana.setBounds(20, 185,300, 20);
		add(diaDaSemana);

		JComboBox<ModalidadeEnum> comboBoxModalidade = new JComboBox<>(ModalidadeEnum.values());
		comboBoxModalidade.setBounds(20, 241, 231, 20);
		add(comboBoxModalidade);
		
		JComboBox<IntensidadeEnum> comboBoxIntensidade= new JComboBox<>(IntensidadeEnum.values());
		comboBoxIntensidade.setBounds(20, 353, 231, 20);
		add(comboBoxIntensidade);
		
	}

	private void adicionarBotaoCadastrar() {
		cadastrar = new JButton("Cadastrar");
		cadastrar.setFont(new Font("Dialog", Font.BOLD, 12));
		cadastrar.setForeground(Color.WHITE);
		cadastrar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

			}
		});
		cadastrar.setBackground(new Color(102, 204, 255));
		cadastrar.setBounds(330, 450, 100, 23);
		add(cadastrar);

	}

	private void adicionarBotaoLimpar() {
		limpar = new JButton("Limpar");
		limpar.setFont(new Font("Dialog", Font.BOLD, 12));
		limpar.setForeground(Color.WHITE);
		limpar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int resp = JOptionPane.showConfirmDialog(null, "Realmente deseja limpar o formulario", "Limpa",
						JOptionPane.YES_NO_OPTION);
				if (resp == JOptionPane.YES_OPTION) {
					limparCampos();
				}
			}
		});
		limpar.setBackground(new Color(102, 204, 255));
		limpar.setBounds(227, 450, 89, 23);
		add(limpar);
	}

	private void limparCampos() {
		campoNomeAtividade.setText(null);
	}
}
