package br.ucsal.bes.poo20181.academia.gui.instrutor;

import java.awt.Font;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;

import br.ucsal.bes.poo20181.academia.domain.Pessoa;
import br.ucsal.bes.poo20181.academia.gui.principais.PainelEditarDados;
import br.ucsal.bes.poo20181.academia.util.ModalidadeEnum;

public class PainelEditarDadosProfessor extends PainelEditarDados {

	private static final long serialVersionUID = 1L;

	public PainelEditarDadosProfessor(Pessoa usuario) {
		super(usuario);

	}

	@Override
	protected void adicionarLabelsAtividade() {
		JLabel dadosProfissionais = new JLabel("DADOS PROFISSIONAIS:");
		dadosProfissionais.setFont(new Font("Dialog", Font.BOLD, 12));
		dadosProfissionais.setBounds(10, 270, 148, 14);
		add(dadosProfissionais);

		JLabel formacao = new JLabel("Forma��o:");
		formacao.setFont(new Font("Dialog", Font.BOLD, 12));
		formacao.setBounds(10, 300, 61, 22);
		add(formacao);

		JLabel modalidade = new JLabel("Modalidade:");
		modalidade.setFont(new Font("Dialog", Font.BOLD, 12));
		modalidade.setBounds(260, 300, 83, 14);
		add(modalidade);
	}

	@Override
	protected void adicionarCamposAtividade() {

		campoFormacao = new JTextField();
		campoFormacao.setColumns(10);
		campoFormacao.setBounds(72, 300, 165, 20);
		add(campoFormacao);

		JComboBox<ModalidadeEnum> comboBoxModalidade = new JComboBox<>(ModalidadeEnum.values());
		comboBoxModalidade.setBounds(333, 300, 210, 20);
		add(comboBoxModalidade);
	}

}
