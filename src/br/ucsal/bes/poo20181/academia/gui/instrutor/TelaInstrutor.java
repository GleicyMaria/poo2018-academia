package br.ucsal.bes.poo20181.academia.gui.instrutor;

import java.awt.Color;
import java.awt.Font;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import br.ucsal.bes.poo20181.academia.domain.Professor;
import br.ucsal.bes.poo20181.academia.gui.TelaLogin;
import br.ucsal.bes.poo20181.academia.gui.cliente.TelaCliente;
import br.ucsal.bes.poo20181.academia.gui.principais.PainelAcompanhamento;
import br.ucsal.bes.poo20181.academia.gui.principais.PainelEditarDados;

public class TelaInstrutor {

	public JFrame janelaInstru;

	private JPanel painelPrincipal, painelLateral;
	private JButton botaoCadastAtiv, botaoEditDados, botaoAcompanhamento, logout;
	private JScrollPane scroll;
	private int xx, xy;
	private PainelCadastAtividade cadastroAtivi;
	private PainelEditarDadosProfessor editarDados;
	private PainelAcompanhamentoProfessor acompanha;
	private Professor usuario;

	public static void main(String[] args) {
		TelaInstrutor tela = new TelaInstrutor();
		tela.janelaInstru.setVisible(true);

	}

	public TelaInstrutor() {

		prepararJanela();
		prepararPainelPrincipal();
		iniciarPainel();
		prepararPainelLateral();
		prepararBtnAcompanhamento();
		prepararBotaoCadastAtiv();
		prepararBtnEditarDados();
		prepararIdentificador();
		prepararBtnFechar();
		prepararBtnMinimizar();
		prepararBtnLogout();

	}

	public TelaInstrutor(Professor usuario) {
        this.usuario = usuario;
		prepararJanela();
		prepararPainelPrincipal();
		iniciarPainel();
		prepararPainelLateral();
		prepararBtnAcompanhamento();
		prepararBotaoCadastAtiv();
		prepararBtnEditarDados();
		prepararIdentificador();
		prepararBtnFechar();
		prepararBtnMinimizar();
		prepararBtnLogout();
	}

	private void prepararJanela() {
		janelaInstru = new JFrame();
		janelaInstru.setUndecorated(true);
		janelaInstru.setResizable(false);
		janelaInstru.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		janelaInstru.setBounds(100, 100, 799, 465);

	}

	private void prepararPainelPrincipal() {
		painelPrincipal = new JPanel();
		painelPrincipal.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {

				xx = e.getX();
				xy = e.getY();
			}
		});
		painelPrincipal.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent arg0) {
				int x = arg0.getXOnScreen();
				int y = arg0.getYOnScreen();

				janelaInstru.setLocation(x - xx, y - xy);

			}
		});
		painelPrincipal.setBackground(Color.WHITE);
		painelPrincipal.setBorder(new EmptyBorder(5, 5, 5, 5));
		janelaInstru.setContentPane(painelPrincipal);
		painelPrincipal.setLayout(null);

	}

	private void prepararPainelLateral() {
		painelLateral = new JPanel();
		painelLateral.setBounds(0, 0, 225, 465);
		painelLateral.setForeground(new Color(0, 0, 0));
		painelLateral.setBackground(new Color(102, 102, 102));
		painelPrincipal.add(painelLateral);
		painelLateral.setLayout(null);

	}

	private void iniciarPainel() {
		cadastroAtivi = new PainelCadastAtividade();
		editarDados = new PainelEditarDadosProfessor(usuario);
		acompanha = new PainelAcompanhamentoProfessor();
		scroll = new JScrollPane();
		scroll.setBounds(225, 23, 574, 465);

		painelPrincipal.add(scroll);

	}

	private void prepararBtnAcompanhamento() {

		botaoAcompanhamento = new JButton();
		
		botaoAcompanhamento.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource().equals(botaoAcompanhamento)) {
					scroll.setViewportView(acompanha);
				}
			}

		});

			botaoAcompanhamento.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				setColor(botaoAcompanhamento);
				resetColor(botaoCadastAtiv);
				resetColor(botaoEditDados);
				resetColor(botaoAcompanhamento);
				resetColor(logout);

			}
		});
		botaoAcompanhamento.setBackground(new Color(102, 102, 102));
		botaoAcompanhamento.setBounds(0, 96, 225, 35);
		painelLateral.add(botaoAcompanhamento);
		botaoAcompanhamento.setLayout(null);

		JLabel textBtnAcompa = new JLabel("Acompanhamento ");
		textBtnAcompa.setForeground(new Color(255, 255, 255));
		textBtnAcompa.setFont(new Font("Dialog", Font.BOLD, 16));
		textBtnAcompa.setBounds(45, 11, 150, 20);
		botaoAcompanhamento.add(textBtnAcompa);

		JLabel iconeAcompa = new JLabel("");
		iconeAcompa.setIcon(new ImageIcon(
				TelaInstrutor.class.getResource("/br/ucsal/bes/poo20181/academia/gui/img/Inspection.png")));
		iconeAcompa.setBounds(10, 8, 25, 22);
		botaoAcompanhamento.add(iconeAcompa);
	}

	private void prepararBotaoCadastAtiv() {

		botaoCadastAtiv = new JButton();
		botaoCadastAtiv.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource().equals(botaoCadastAtiv)) {
					scroll.setViewportView(cadastroAtivi);
				}
			}

		});

		botaoCadastAtiv.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {

				setColor(botaoCadastAtiv);
				resetColor(botaoAcompanhamento);
				resetColor(botaoEditDados);
				resetColor(logout);

			}
		});

		botaoCadastAtiv.setLayout(null);
		botaoCadastAtiv.setBackground(new Color(102, 102, 102));
		botaoCadastAtiv.setBounds(0, 132, 225, 35);
		painelLateral.add(botaoCadastAtiv);

		JLabel txtBtnCadasAtiv = new JLabel("Cadastrar Atividade ");
		txtBtnCadasAtiv.setForeground(Color.WHITE);
		txtBtnCadasAtiv.setFont(new Font("Dialog", Font.BOLD, 16));
		txtBtnCadasAtiv.setBounds(45, 11, 200, 14);
		botaoCadastAtiv.add(txtBtnCadasAtiv);

		JLabel iconeCadasAtiv = new JLabel("");
		iconeCadasAtiv.setIcon(
				new ImageIcon(TelaInstrutor.class.getResource("/br/ucsal/bes/poo20181/academia/gui/img/Barbell.png")));
		iconeCadasAtiv.setBounds(10, 8, 25, 22);
		botaoCadastAtiv.add(iconeCadasAtiv);

	}

	private void prepararBtnEditarDados() {
		botaoEditDados = new JButton();

		botaoEditDados.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource().equals(botaoEditDados)) {
					scroll.setViewportView(editarDados);
				}
			}

		});

		botaoEditDados.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {

				setColor(botaoEditDados);
				resetColor(botaoAcompanhamento);
				resetColor(botaoCadastAtiv);
				resetColor(logout);
			}
		});
		botaoEditDados.setLayout(null);
		botaoEditDados.setBackground(new Color(102, 102, 102));
		botaoEditDados.setBounds(0, 168, 225, 35);
		painelLateral.add(botaoEditDados);

		JLabel txtBtnEditDados = new JLabel("Editar Dados");
		txtBtnEditDados.setForeground(Color.WHITE);
		txtBtnEditDados.setFont(new Font("Dialog", Font.BOLD, 16));
		txtBtnEditDados.setBounds(45, 11, 150, 14);
		botaoEditDados.add(txtBtnEditDados);

		JLabel iconeEditDados = new JLabel("");
		iconeEditDados.setIcon(
				new ImageIcon(TelaInstrutor.class.getResource("/br/ucsal/bes/poo20181/academia/gui/img/Create.png")));
		iconeEditDados.setBounds(10, 8, 25, 22);
		botaoEditDados.add(iconeEditDados);

	}

	private void prepararIdentificador() {
		JLabel identificador = new JLabel("Instrutor : " + usuario.getNome());
		identificador.setForeground(new Color(255, 255, 255));
		identificador.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 16));
		identificador.setBounds(10, 399, 250, 22);
		painelLateral.add(identificador);

	}

	private void prepararBtnLogout() {
		logout = new JButton();
		logout.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {

				setColor(logout);
				resetColor(botaoAcompanhamento);
				resetColor(botaoEditDados);
				resetColor(botaoCadastAtiv);

				int resp = JOptionPane.showConfirmDialog(null, "Deseja realmente fazer logout?", "Logout",
						JOptionPane.YES_NO_OPTION);

				if (resp == JOptionPane.YES_OPTION) {
					janelaInstru.setVisible(false);
					TelaLogin.main(null);
				}

			}
		});
		logout.setBackground(new Color(102, 102, 102));
		logout.setBounds(10, 432, 45, 22);
		painelLateral.add(logout);
		logout.setLayout(null);

		JLabel txtBtnLogout = new JLabel("Logout");
		txtBtnLogout.setBounds(0, 0, 39, 15);
		txtBtnLogout.setForeground(new Color(255, 255, 255));
		txtBtnLogout.setFont(new Font("Dialog", Font.BOLD, 11));
		logout.add(txtBtnLogout);
	}

	private void prepararBtnMinimizar() {
		JLabel minimi = new JLabel("");
		minimi.setBounds(745, 0, 25, 22);
		minimi.setIcon(
				new ImageIcon(TelaCliente.class.getResource("/br/ucsal/bes/poo20181/academia/gui/img/Minimize.png")));
		minimi.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getSource().equals(minimi)) {
					janelaInstru.setState(Frame.ICONIFIED);
				}
			}
		});
		minimi.setForeground(new Color(102, 204, 255));
		minimi.setFont(new Font("Tahoma", Font.BOLD, 15));
		painelPrincipal.add(minimi);

	}

	private void prepararBtnFechar() {
		JLabel fechar = new JLabel("");
		fechar.setBounds(774, 0, 25, 22);
		fechar.setIcon(new ImageIcon(
				TelaInstrutor.class.getResource("/br/ucsal/bes/poo20181/academia/gui/img/Close Window.png")));
		fechar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				System.exit(0);
			}
		});
		fechar.setForeground(new Color(102, 204, 255));
		fechar.setFont(new Font("Tahoma", Font.BOLD, 15));
		painelPrincipal.add(fechar);

	}

	private void setColor(JButton button) {

		button.setBackground(new Color(153, 153, 153));

	}

	private void resetColor(JButton button) {

		button.setBackground(new Color(102, 102, 102));

	}

}
