package br.ucsal.bes.poo20181.academia.gui.instrutor;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;

import br.ucsal.bes.poo20181.academia.gui.principais.PainelAcompanhamento;

public class PainelAcompanhamentoProfessor extends PainelAcompanhamento{

	private static final long serialVersionUID = 1L;

	public PainelAcompanhamentoProfessor() {
		super();
		prepararAcompanhamento();
		
	}
	
	@Override
	public void prepararAcompanhamento() {

		JPanel painelSuperior = new JPanel();
		painelSuperior.setBackground(new Color(51, 204, 255));
		painelSuperior.setBounds(0, 0, 574, 56);
		add(painelSuperior);
		painelSuperior.setLayout(null);

		JLabel cadastrarAtividade = new JLabel("Painel de Acompanhamento dos Alunos");
		cadastrarAtividade.setBounds(10, 11, 400, 34);
		cadastrarAtividade.setForeground(Color.WHITE);
		cadastrarAtividade.setFont(new Font("Dialog", Font.BOLD, 16));
		painelSuperior.add(cadastrarAtividade);

		adicionarLabels();
		adicionarTable();
		adicionarComboBox();
		
	}

}
