package br.ucsal.bes.poo20181.academia.gui.principais;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.MaskFormatter;

import br.ucsal.bes.poo20181.academia.controller.AlunoController;
import br.ucsal.bes.poo20181.academia.controller.ProfessorController;
import br.ucsal.bes.poo20181.academia.domain.Cliente;
import br.ucsal.bes.poo20181.academia.domain.Endereco;
import br.ucsal.bes.poo20181.academia.domain.Pessoa;
import br.ucsal.bes.poo20181.academia.domain.Professor;
import br.ucsal.bes.poo20181.academia.util.ModalidadeEnum;
import br.ucsal.bes.poo20181.academia.util.SexoEnum;

public class PainelEditarDados extends JPanel {
	
	protected static final long serialVersionUID = 1L;
	protected JTextField campoNome, campoMatricula, campoDataCadastro, campoCPF, campoDataNascimento, campoTel,
			campoEmail, campoFormacao, campoRua, campoCEP, campoNumero, campoCidade, campoBairro;
	protected JButton modificar, limpar;
	protected JComboBox<SexoEnum> comboBoxSexo;
	
	protected Pessoa usuario;
	protected final SimpleDateFormat DF = new SimpleDateFormat("dd/MM/yyyy");
	
	

	public PainelEditarDados(Pessoa usuario) {
        this.usuario = usuario;
		setBackground(new Color(255, 255, 255));
		setLayout(null);
		setPreferredSize(new Dimension(1000, 465));
		prepararcadastrarAluno();
	}

	protected void prepararcadastrarAluno() {

		JPanel painelSuperior = new JPanel();
		painelSuperior.setBackground(new Color(51, 204, 255));
		painelSuperior.setBounds(0, 0, 574, 56);
		add(painelSuperior);
		painelSuperior.setLayout(null);

		JLabel cadastrarAluno = new JLabel("Editar Dados");
		cadastrarAluno.setBounds(10, 11, 178, 34);
		cadastrarAluno.setForeground(Color.WHITE);
		cadastrarAluno.setFont(new Font("Dialog", Font.BOLD, 16));
		painelSuperior.add(cadastrarAluno);

		prepararLabels();
		prepararCampos();
		prepararBotoes();
	}

	protected void prepararLabels() {

		adicionarLabelsDados();
		adicionarLabelsContato();
		adicionarLabelsAtividade();
		adicionarLabelsEndereco();
	}

	protected void prepararCampos() {

		adicionarCamposDados();
		adicionarCamposContato();
		adicionarCamposAtividade();
		adicionarCamposEndereco();

	}

	protected void prepararBotoes() {
		adicionarBotaoCadastrar();
		adicionarBotaoLimpar();
	}

	protected void adicionarLabelsDados() {
		JLabel dados = new JLabel("DADOS:");
		dados.setFont(new Font("Dialog", Font.BOLD, 12));
		dados.setBounds(10, 70, 61, 14);
		add(dados);

		JLabel nome = new JLabel("Nome:");
		nome.setFont(new Font("Dialog", Font.BOLD, 12));
		nome.setBounds(10, 100, 61, 14);
		add(nome);

		JLabel cpf = new JLabel("CPF:");
		cpf.setFont(new Font("Dialog", Font.BOLD, 12));
		cpf.setBounds(10, 130, 61, 14);
		add(cpf);

		JLabel sexo = new JLabel("Sexo:");
		sexo.setFont(new Font("Dialog", Font.BOLD, 12));
		sexo.setBounds(10, 160, 61, 14);
		add(sexo);

		JLabel dataDeNascimento = new JLabel("Data de Nascimento:");
		dataDeNascimento.setFont(new Font("Dialog", Font.BOLD, 12));
		dataDeNascimento.setBounds(260, 160, 123, 14);
		add(dataDeNascimento);

	}

	protected void adicionarCamposDados() {
		campoNome = new JTextField(usuario.getNome());
		campoNome.setColumns(10);
		campoNome.setBounds(55, 100, 488, 20);
		add(campoNome);

		campoCPF = new JTextField(String.format(usuario.getCpf().toString()));
		campoCPF.setColumns(10);
		campoCPF.setBounds(55, 130, 178, 20);
		add(campoCPF);

		comboBoxSexo = new JComboBox<>(SexoEnum.values());
		comboBoxSexo.setBounds(55, 160, 178, 20);
		add(comboBoxSexo);

		campoDataNascimento = new JTextField(DF.format(usuario.getDataNascimento()));
		campoDataNascimento.setColumns(10);
		campoDataNascimento.setBounds(386, 160, 157, 20);
		add(campoDataNascimento);

	}

	protected void adicionarLabelsContato() {
		JLabel contato = new JLabel("CONTATO:");
		contato.setFont(new Font("Dialog", Font.BOLD, 12));
		contato.setBounds(10, 200, 61, 14);
		add(contato);

		JLabel email = new JLabel("E-mail:");
		email.setFont(new Font("Dialog", Font.BOLD, 12));
		email.setBounds(260, 230, 38, 14);
		add(email);

		JLabel tel = new JLabel("Tel.:");
		tel.setFont(new Font("Dialog", Font.BOLD, 12));
		tel.setBounds(10, 230, 61, 14);
		add(tel);
	}

	protected void adicionarCamposContato() {
		campoTel = new JTextField(usuario.getTelefone());
		campoTel.setColumns(10);
		campoTel.setBounds(55, 230, 182, 20);
		add(campoTel);

		campoEmail = new JTextField(usuario.getEmail());
		campoEmail.setColumns(10);
		campoEmail.setBounds(322, 230, 221, 20);
		add(campoEmail);
	}

	protected void adicionarLabelsAtividade() {
		JLabel atividade = new JLabel("ATIVIDADE A SER PRATICADA  :");
		atividade.setFont(new Font("Dialog", Font.BOLD, 12));
		atividade.setBounds(10, 270, 200, 14);
		add(atividade);

		JLabel modalidade = new JLabel("Modalidade:");
		modalidade.setFont(new Font("Dialog", Font.BOLD, 12));
		modalidade.setBounds(10, 300, 83, 14);
		add(modalidade);
	}

	protected void adicionarCamposAtividade() {
		

		JComboBox<ModalidadeEnum> comboBoxModalidade = new JComboBox<>(ModalidadeEnum.values());
		comboBoxModalidade.setBounds(79, 295, 210, 20);
		add(comboBoxModalidade);
	}

	protected void adicionarLabelsEndereco() {
		JLabel endereco = new JLabel("ENDERE�O:");
		endereco.setFont(new Font("Dialog", Font.BOLD, 12));
		endereco.setBounds(10, 330, 76, 14);
		add(endereco);

		JLabel rua = new JLabel("Rua:");
		rua.setFont(new Font("Dialog", Font.BOLD, 12));
		rua.setBounds(10, 360, 32, 14);
		add(rua);

		JLabel numero = new JLabel("N�");
		numero.setFont(new Font("Dialog", Font.BOLD, 12));
		numero.setBounds(247, 360, 32, 14);
		add(numero);

		JLabel bairro = new JLabel("Bairro:");
		bairro.setFont(new Font("Dialog", Font.BOLD, 12));
		bairro.setBounds(333, 360, 48, 14);
		add(bairro);

		JLabel cidade = new JLabel("Cidade:");
		cidade.setFont(new Font("Dialog", Font.BOLD, 12));
		cidade.setBounds(247, 390, 48, 14);
		add(cidade);

		JLabel cep = new JLabel("CEP:");
		cep.setFont(new Font("Dialog", Font.BOLD, 12));
		cep.setBounds(10, 390, 48, 14);
		add(cep);
	}

	protected void adicionarCamposEndereco() {
		campoRua = new JTextField(usuario.getEndereco().getRua());
		campoRua.setColumns(10);
		campoRua.setBounds(40, 360, 197, 20);
		add(campoRua);

		campoCEP = new JTextField(usuario.getEndereco().getCep());
		campoCEP.setColumns(10);
		campoCEP.setBounds(40, 390, 197, 20);
		add(campoCEP);
		campoNumero = new JTextField("11");
		campoNumero.setColumns(10);
		campoNumero.setBounds(275, 360, 48, 20);
		add(campoNumero);

		campoCidade = new JTextField(""+ usuario.getEndereco().getCidade());
		campoCidade.setColumns(10);
		campoCidade.setBounds(296, 390, 248, 20);
		add(campoCidade);

		campoBairro = new JTextField(usuario.getEndereco().getBairro());
		campoBairro.setColumns(10);
		campoBairro.setBounds(386, 360, 155, 20);
		add(campoBairro);

	}

	protected void adicionarBotaoCadastrar() {
		modificar = new JButton("Modificar");
		modificar.setFont(new Font("Dialog", Font.BOLD, 12));
		modificar.setForeground(Color.WHITE);
		modificar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
                Endereco end = new Endereco(campoRua.getText(), campoBairro.getText(),Integer.parseInt(campoNumero.getText()),
                        campoCidade.getText(), campoCEP.getText());

			    switch (usuario.getPerfil()){
                    case PROFESSOR:
                        Professor professor = new Professor(campoNome.getText(), new Date(), end, "Eng. de Software", ModalidadeEnum.MUSCULACAO);
                        professor.setCpf(campoCPF.getText());
                        ProfessorController.alterarDados(professor);
                        break;
                    case ALUNO:
                        Cliente aluno = new Cliente(campoNome.getText(), campoCPF.getText(), new Date(), end, campoEmail.getText(), (SexoEnum) comboBoxSexo.getSelectedItem());
                        aluno.setCpf(campoCPF.getText());
                        AlunoController.alterarDados(aluno);
                    default:
                        System.out.println("Nada...");
                }



                JOptionPane.showMessageDialog(null, "Altera��o dos dados do instrutor realizado com sucesso!");
			}
		});
		modificar.setBackground(new Color(102, 204, 255));
		modificar.setBounds(442, 420, 100, 20);
		add(modificar);

	}

	protected void adicionarBotaoLimpar() {
		limpar = new JButton("Limpar");
		limpar.setFont(new Font("Dialog", Font.BOLD, 12));
		limpar.setForeground(Color.WHITE);
		limpar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int resp = JOptionPane.showConfirmDialog(null, "Realmente deseja limpar o formulario", "Limpa",
						JOptionPane.YES_NO_OPTION);

				if (resp == JOptionPane.YES_OPTION) {
					limparCampos();
				}
			}
		});
		limpar.setBackground(new Color(102, 204, 255));
		limpar.setBounds(325, 420, 100, 20);
		add(limpar);

	}

	protected void limparCampos() {
		campoNome.setText(null);
		campoDataNascimento.setText(null);
		campoCPF.setText(null);
		campoMatricula.setText(null);
		campoDataCadastro.setText(null);
		campoTel.setText(null);
		campoEmail.setText(null);
		campoFormacao.setText(null);
		campoRua.setText(null);
		campoCEP.setText(null);
		campoNumero.setText(null);
		campoCidade.setText(null);
		campoBairro.setText(null);
	}

}


