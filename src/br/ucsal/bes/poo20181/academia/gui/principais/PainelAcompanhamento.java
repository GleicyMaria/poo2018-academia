package br.ucsal.bes.poo20181.academia.gui.principais;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class PainelAcompanhamento extends JPanel {

	private static final long serialVersionUID = 1L;
	
	public PainelAcompanhamento() {
		setBackground(new Color(255, 255, 255));
		setLayout(null);
		setPreferredSize(new Dimension(1000, 500));
		prepararAcompanhamento();
	}

	public void prepararAcompanhamento() {

		JPanel painelSuperior = new JPanel();
		painelSuperior.setBackground(new Color(51, 204, 255));
		painelSuperior.setBounds(0, 0, 574, 56);
		add(painelSuperior);
		painelSuperior.setLayout(null);

		JLabel cadastrarAtividade = new JLabel("Painel de Acompanhamento");
		cadastrarAtividade.setBounds(10, 11, 400, 34);
		cadastrarAtividade.setForeground(Color.WHITE);
		cadastrarAtividade.setFont(new Font("Dialog", Font.BOLD, 16));
		painelSuperior.add(cadastrarAtividade);

		adicionarLabels();
		adicionarTable();
		adicionarComboBox();
		
	}



	public void adicionarLabels() {
		JLabel txtTurma = new JLabel("Turma: ");
		txtTurma.setFont(new Font("Dialog", Font.BOLD, 12));
		txtTurma.setBounds(10, 85, 200, 14);
		add(txtTurma);

		JLabel txtAluno = new JLabel(" Aluno ");
		txtAluno.setFont(new Font("Dialog", Font.BOLD, 12));
		txtAluno.setBounds(10, 141, 250, 14);
		add(txtAluno);
		
		JLabel txtAtividade = new JLabel("Atividades");
		txtAtividade.setFont(new Font("Dialog", Font.BOLD, 12));
		txtAtividade.setBounds(10, 197, 90, 14);
		add(txtAtividade);

		
		
		
	}

	public void adicionarTable() {
		

	}

	public void adicionarComboBox() {

		JComboBox <String> comboBoxTurma = new JComboBox<String>();
		comboBoxTurma.setBounds(20, 110, 442, 20);
		add(comboBoxTurma);
		
		JComboBox<String> comboBoxAluno = new JComboBox<>();
		comboBoxAluno.setBounds(20, 166,300, 20);
		add(comboBoxAluno);

		
		
	}

	
}
