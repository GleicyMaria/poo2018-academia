package br.ucsal.bes.poo20181.academia.gui.principais;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.MaskFormatter;

import br.ucsal.bes.poo20181.academia.controller.AlunoController;
import br.ucsal.bes.poo20181.academia.util.ModalidadeEnum;
import br.ucsal.bes.poo20181.academia.util.PeriodoEnum;
import br.ucsal.bes.poo20181.academia.util.SexoEnum;

public class PainelCadastro extends JPanel {

	protected static final long serialVersionUID = 1L;
	protected JTextField campoNome, campoMatricula, campoDataCadastro, campoCPF, campoDataNascimento, campoTel,
			campoEmail, campoFormacao, campoRua, campoCEP, campoNumero, campoCidade, campoBairro;
	protected JButton botaoCadastrar, limpar;
	protected JComboBox<SexoEnum> comboBoxSexo;
	protected JComboBox<ModalidadeEnum> comboBoxModalidade;
	protected JComboBox<PeriodoEnum> comboBoxPerido;
	protected final SimpleDateFormat DF = new SimpleDateFormat("dd/MM/yyyy");
	protected JPanel painelSuperior;
	protected JLabel cadastrar, dados, nome, cpf, sexo, dataDeNascimento, contato, email, tel, atividade,
			modalidade, periodo , endereco, rua, numero, bairro, cidade, cep;

	public PainelCadastro() throws ParseException {
		setBackground(new Color(255, 255, 255));
		setLayout(null);
		setPreferredSize(new Dimension(1000, 465));
		prepararCadasto();
	}

	protected void prepararCadasto() throws ParseException {

		painelSuperior = new JPanel();
		painelSuperior.setBackground(new Color(51, 204, 255));
		painelSuperior.setBounds(0, 0, 574, 56);
		add(painelSuperior);
		painelSuperior.setLayout(null);

		cadastrar = new JLabel("Cadastrar Cliente");
		cadastrar.setBounds(10, 11, 178, 34);
		cadastrar.setForeground(Color.WHITE);
		cadastrar.setFont(new Font("Dialog", Font.BOLD, 16));
		painelSuperior.add(cadastrar);

		prepararLabels();
		prepararCampos();
		prepararBotoes();
	}

	protected void prepararLabels() {

		adicionarLabelsDados();
		adicionarLabelsContato();
		adicionarLabelsAtividade();
		adicionarLabelsEndereco();
	}

	protected void prepararCampos() throws ParseException {

		adicionarCamposDados();
		adicionarCamposContato();
		adicionarCamposAtividade();
		adicionarCamposEndereco();

	}

	protected void prepararBotoes() {
		adicionarBotaobotaoCadastrar();
		adicionarBotaoLimpar();
	}

	protected void adicionarLabelsDados() {
		dados = new JLabel("DADOS:");
		dados.setFont(new Font("Dialog", Font.BOLD, 12));
		dados.setBounds(10, 70, 61, 14);
		add(dados);

		nome = new JLabel("Nome:");
		nome.setFont(new Font("Dialog", Font.BOLD, 12));
		nome.setBounds(10, 100, 61, 14);
		add(nome);

		cpf = new JLabel("CPF:");
		cpf.setFont(new Font("Dialog", Font.BOLD, 12));
		cpf.setBounds(10, 130, 61, 14);
		add(cpf);

		sexo = new JLabel("Sexo:");
		sexo.setFont(new Font("Dialog", Font.BOLD, 12));
		sexo.setBounds(10, 160, 61, 14);
		add(sexo);

		dataDeNascimento = new JLabel("Data de Nascimento:");
		dataDeNascimento.setFont(new Font("Dialog", Font.BOLD, 12));
		dataDeNascimento.setBounds(260, 160, 123, 14);
		add(dataDeNascimento);

	}

	protected void adicionarCamposDados() throws ParseException {
		campoNome = new JTextField();
		campoNome.setColumns(10);
		campoNome.setBounds(55, 100, 488, 20);
		add(campoNome);

		campoCPF = new JFormattedTextField(new MaskFormatter("###.###.###-##"));
		campoCPF.setColumns(10);
		campoCPF.setBounds(55, 130, 178, 20);
		add(campoCPF);

		comboBoxSexo = new JComboBox<>(SexoEnum.values());
		comboBoxSexo.setBounds(55, 160, 178, 20);
		add(comboBoxSexo);

		campoDataNascimento = new JFormattedTextField(new MaskFormatter("##/##/####"));
		campoDataNascimento.setColumns(10);
		campoDataNascimento.setBounds(386, 160, 157, 20);
		add(campoDataNascimento);

	}

	protected void adicionarLabelsContato() {
		contato = new JLabel("CONTATO:");
		contato.setFont(new Font("Dialog", Font.BOLD, 12));
		contato.setBounds(10, 200, 61, 14);
		add(contato);

		email = new JLabel("E-mail:");
		email.setFont(new Font("Dialog", Font.BOLD, 12));
		email.setBounds(260, 230, 38, 14);
		add(email);

		tel = new JLabel("Tel.:");
		tel.setFont(new Font("Dialog", Font.BOLD, 12));
		tel.setBounds(10, 230, 61, 14);
		add(tel);
	}

	protected void adicionarCamposContato() throws ParseException {
		campoTel = new JFormattedTextField(new MaskFormatter("(##)#####-####"));
		campoTel.setColumns(10);
		campoTel.setBounds(55, 230, 182, 20);
		add(campoTel);

		campoEmail = new JTextField();
		campoEmail.setColumns(10);
		campoEmail.setBounds(322, 230, 221, 20);
		add(campoEmail);
	}

	protected void adicionarLabelsAtividade() {
		atividade = new JLabel("ATIVIDADE A SER PRATICADA  :");
		atividade.setFont(new Font("Dialog", Font.BOLD, 12));
		atividade.setBounds(10, 270, 200, 14);
		add(atividade);

		modalidade = new JLabel("Modalidade:");
		modalidade.setFont(new Font("Dialog", Font.BOLD, 12));
		modalidade.setBounds(10, 300, 83, 14);
		add(modalidade);
		
		periodo = new JLabel("Periodo: ");
		periodo.setFont(new Font("Dialog", Font.BOLD, 12));
		periodo.setBounds(294, 300, 83, 14);
		add(periodo);
		
				
	}

	protected void adicionarCamposAtividade() {

		comboBoxModalidade = new JComboBox<>(ModalidadeEnum.values());
		comboBoxModalidade.setBounds(79, 295, 210, 20);
		add(comboBoxModalidade);
		
		comboBoxPerido= new JComboBox<>(PeriodoEnum.values());
		comboBoxPerido.setBounds(355, 295, 100, 20);
		add(comboBoxPerido);
	}

	protected void adicionarLabelsEndereco() {
		endereco = new JLabel("ENDERE�O:");
		endereco.setFont(new Font("Dialog", Font.BOLD, 12));
		endereco.setBounds(10, 330, 76, 14);
		add(endereco);

		rua = new JLabel("Rua:");
		rua.setFont(new Font("Dialog", Font.BOLD, 12));
		rua.setBounds(10, 360, 32, 14);
		add(rua);

		numero = new JLabel("N�");
		numero.setFont(new Font("Dialog", Font.BOLD, 12));
		numero.setBounds(247, 360, 32, 14);
		add(numero);

		bairro = new JLabel("Bairro:");
		bairro.setFont(new Font("Dialog", Font.BOLD, 12));
		bairro.setBounds(333, 360, 48, 14);
		add(bairro);

		cidade = new JLabel("Cidade:");
		cidade.setFont(new Font("Dialog", Font.BOLD, 12));
		cidade.setBounds(247, 390, 48, 14);
		add(cidade);

		cep = new JLabel("CEP:");
		cep.setFont(new Font("Dialog", Font.BOLD, 12));
		cep.setBounds(10, 390, 48, 14);
		add(cep);
	}

	protected void adicionarCamposEndereco() throws ParseException {
		campoRua = new JTextField();
		campoRua.setColumns(10);
		campoRua.setBounds(40, 360, 197, 20);
		add(campoRua);

		campoCEP = new JFormattedTextField(new MaskFormatter("#####-###"));
		campoCEP.setColumns(10);
		campoCEP.setBounds(40, 390, 197, 20);
		add(campoCEP);

		campoNumero = new JTextField();
		campoNumero.setColumns(10);
		campoNumero.setBounds(275, 360, 48, 20);
		add(campoNumero);

		campoCidade = new JTextField();
		campoCidade.setColumns(10);
		campoCidade.setBounds(296, 390, 248, 20);
		add(campoCidade);

		campoBairro = new JTextField();
		campoBairro.setColumns(10);
		campoBairro.setBounds(386, 360, 155, 20);
		add(campoBairro);

	}

	protected void adicionarBotaobotaoCadastrar() {
		botaoCadastrar = new JButton("botaoCadastrar");
		botaoCadastrar.setFont(new Font("Dialog", Font.BOLD, 12));
		botaoCadastrar.setForeground(Color.WHITE);
		botaoCadastrar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				AlunoController.cadastrarEndereco(campoRua.getText(), campoNumero.getText(), campoBairro.getText(),
						campoCidade.getText(), campoCEP.getText());
				try {
					AlunoController.cadastrarDados(campoNome.getText(), campoCPF.getText(),
							DF.parse(campoDataNascimento.getText()), campoEmail.getText(),
							(SexoEnum) comboBoxSexo.getSelectedItem());
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				JOptionPane.showMessageDialog(null, "Cadastro de instrutor realizado com sucesso!");
			}
		});
		botaoCadastrar.setBackground(new Color(102, 204, 255));
		botaoCadastrar.setBounds(442, 420, 100, 20);
		add(botaoCadastrar);

	}

	protected void adicionarBotaoLimpar() {
		limpar = new JButton("Limpar");
		limpar.setFont(new Font("Dialog", Font.BOLD, 12));
		limpar.setForeground(Color.WHITE);
		limpar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int resp = JOptionPane.showConfirmDialog(null, "Realmente deseja limpar o formulario", "Limpa",
						JOptionPane.YES_NO_OPTION);

				if (resp == JOptionPane.YES_OPTION) {
					limparCampos();
				}
			}
		});
		limpar.setBackground(new Color(102, 204, 255));
		limpar.setBounds(325, 420, 100, 20);
		add(limpar);

	}

	protected void limparCampos() {
		campoNome.setText(null);
		campoDataNascimento.setText(null);
		campoCPF.setText(null);
		campoMatricula.setText(null);
		campoDataCadastro.setText(null);
		campoTel.setText(null);
		campoEmail.setText(null);
		campoFormacao.setText(null);
		campoRua.setText(null);
		campoCEP.setText(null);
		campoNumero.setText(null);
		campoCidade.setText(null);
		campoBairro.setText(null);
	}

}
