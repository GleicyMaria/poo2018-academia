package br.ucsal.bes.poo20181.academia.gui.cliente;

import java.awt.Color;
import java.awt.Font;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.text.SimpleDateFormat;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import br.ucsal.bes.poo20181.academia.domain.Cliente;
import br.ucsal.bes.poo20181.academia.domain.Pessoa;
import br.ucsal.bes.poo20181.academia.gui.TelaLogin;
import br.ucsal.bes.poo20181.academia.gui.principais.PainelEditarDados;

public class TelaCliente {

	public JFrame janelaCliente;
    private JPanel painelPrincipal, painelLateral;
    private JButton botaoCadastAtiv, botaoEditDados, botaoAcompanhamento, logout;
    private JScrollPane scroll;
	private int xx, xy;
    private PainelEditarDadosClientes editarDados;
    private PainelAcompanhamentoAluno acompanhamento;
	private Cliente usuario;
	

	public static void main(String[] args) {
		TelaCliente tela = new TelaCliente(null);
		tela.janelaCliente.setVisible(true);

	}

	public TelaCliente(Cliente usuario) {
        this.usuario = usuario;
		prepararJanela();
		prepararPainelPrincipal();
		iniciarPaineis();
		prepararPainelLateral();
		prepararBtnAcompanhamento();
		prepararBtnEditarDados();
		prepararIdentificador();
		prepararBtnFechar();
		prepararBtnMinimizar();
		prepararBtnLogout();

	}

	private void prepararJanela() {
		janelaCliente = new JFrame();
		janelaCliente.setUndecorated(true);
		janelaCliente.setResizable(false);
		janelaCliente.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		janelaCliente.setBounds(100, 100, 799, 465);

	}

	private void prepararPainelPrincipal() {
		painelPrincipal = new JPanel();
		painelPrincipal.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {

				xx = e.getX();
				xy = e.getY();
			}
		});
		painelPrincipal.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent arg0) {
				int x = arg0.getXOnScreen();
				int y = arg0.getYOnScreen();

				janelaCliente.setLocation(x - xx, y - xy);

			}
		});
		painelPrincipal.setBackground(Color.WHITE);
		painelPrincipal.setBorder(new EmptyBorder(5, 5, 5, 5));
		janelaCliente.setContentPane(painelPrincipal);
		painelPrincipal.setLayout(null);

	}

	private void prepararPainelLateral() {
		painelLateral = new JPanel();
		painelLateral.setBounds(0, 0, 225, 465);
		painelLateral.setForeground(new Color(0, 0, 0));
		painelLateral.setBackground(new Color(102, 102, 102));
		painelPrincipal.add(painelLateral);
		painelLateral.setLayout(null);

	}

	private void iniciarPaineis() {
		
		editarDados = new PainelEditarDadosClientes(usuario);
		acompanhamento = new PainelAcompanhamentoAluno();
		
		scroll = new JScrollPane();
		scroll.setBounds(225, 23, 574, 465);

		painelPrincipal.add(scroll);

	}

	private void prepararBtnAcompanhamento() {

		botaoAcompanhamento = new JButton();
		
		botaoAcompanhamento.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource().equals(botaoAcompanhamento)) {
					scroll.setViewportView(acompanhamento);
				}
			}

		});

		botaoAcompanhamento.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				setColor(botaoAcompanhamento);
				resetColor(botaoCadastAtiv);
				resetColor(botaoEditDados);
				resetColor(botaoAcompanhamento);
				resetColor(logout);

			}
		});
		botaoAcompanhamento.setBackground(new Color(102, 102, 102));
		botaoAcompanhamento.setBounds(0, 96, 225, 35);
		painelLateral.add(botaoAcompanhamento);
		botaoAcompanhamento.setLayout(null);

		JLabel textBtnAcompa = new JLabel("Acompanhamento ");
		textBtnAcompa.setForeground(new Color(255, 255, 255));
		textBtnAcompa.setFont(new Font("Dialog", Font.BOLD, 16));
		textBtnAcompa.setBounds(45, 11, 150, 20);
		botaoAcompanhamento.add(textBtnAcompa);

		JLabel iconeAcompa = new JLabel("");
		iconeAcompa.setIcon(
				new ImageIcon(TelaCliente.class.getResource("/br/ucsal/bes/poo20181/academia/gui/img/Inspection.png")));
		iconeAcompa.setBounds(10, 8, 25, 22);
		botaoAcompanhamento.add(iconeAcompa);
	}

	private void prepararBtnEditarDados() {
		botaoEditDados = new JButton();
		botaoEditDados.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource().equals(botaoEditDados)) {
					scroll.setViewportView(editarDados);
				}
			}

		});

		botaoEditDados.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {

				setColor(botaoEditDados);
				resetColor(botaoAcompanhamento);
				resetColor(botaoCadastAtiv);
				resetColor(logout);
			}
		});
		botaoEditDados.setLayout(null);
		botaoEditDados.setBackground(new Color(102, 102, 102));
		botaoEditDados.setBounds(0, 132, 225, 35);
		painelLateral.add(botaoEditDados);

		JLabel txtBtnEditDados = new JLabel("Editar Dados");
		txtBtnEditDados.setForeground(Color.WHITE);
		txtBtnEditDados.setFont(new Font("Dialog", Font.BOLD, 16));
		txtBtnEditDados.setBounds(45, 11, 150, 14);
		botaoEditDados.add(txtBtnEditDados);

		JLabel iconeEditDados = new JLabel("");
		iconeEditDados.setIcon(
				new ImageIcon(TelaCliente.class.getResource("/br/ucsal/bes/poo20181/academia/gui/img/Create.png")));
		iconeEditDados.setBounds(10, 8, 25, 22);
		botaoEditDados.add(iconeEditDados);

	}

	private void prepararIdentificador() {
		JLabel identificador = new JLabel("Cliente : " + usuario.getNome());
		identificador.setForeground(new Color(255, 255, 255));
		identificador.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 16));
		identificador.setBounds(10, 399, 250, 22);
		painelLateral.add(identificador);

	}

	private void prepararBtnLogout() {
		logout = new JButton();
		logout.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {

				setColor(logout);
				resetColor(botaoAcompanhamento);
				resetColor(botaoEditDados);
				resetColor(botaoCadastAtiv);

				int resp = JOptionPane.showConfirmDialog(null, "Deseja realmente fazer logout?", "Logout",
						JOptionPane.YES_NO_OPTION);

				if (resp == JOptionPane.YES_OPTION) {
					janelaCliente.setVisible(false);
					TelaLogin.main(null);
				}

			}
		});
		logout.setBackground(new Color(102, 102, 102));
		logout.setBounds(10, 432, 45, 22);
		painelLateral.add(logout);
		logout.setLayout(null);

		JLabel txtBtnLogout = new JLabel("Logout");
		txtBtnLogout.setBounds(0, 0, 39, 15);
		txtBtnLogout.setForeground(new Color(255, 255, 255));
		txtBtnLogout.setFont(new Font("Dialog", Font.BOLD, 11));
		logout.add(txtBtnLogout);
	}

	private void prepararBtnFechar() {
		JLabel fechar = new JLabel("");
		fechar.setBounds(774, 0, 25, 22);
		fechar.setIcon(new ImageIcon(
				TelaCliente.class.getResource("/br/ucsal/bes/poo20181/academia/gui/img/Close Window.png")));
		fechar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				System.exit(0);
			}
		});
		fechar.setForeground(new Color(102, 204, 255));
		fechar.setFont(new Font("Tahoma", Font.BOLD, 15));
		painelPrincipal.add(fechar);
	}

	private void prepararBtnMinimizar() {
		JLabel minimi = new JLabel("");
		minimi.setBounds(745, 0, 25, 22);
		minimi.setIcon(new ImageIcon(
				TelaCliente.class.getResource("/br/ucsal/bes/poo20181/academia/gui/img/Minimize.png")));
		minimi.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getSource().equals(minimi)) {
					janelaCliente.setState(Frame.ICONIFIED);
				}
			}
		});
		minimi.setForeground(new Color(102, 204, 255));
		minimi.setFont(new Font("Tahoma", Font.BOLD, 15));
		painelPrincipal.add(minimi);

	}

	private void setColor(JButton button) {

		button.setBackground(new Color(153, 153, 153));

	}

	private void resetColor(JButton button) {

		button.setBackground(new Color(102, 102, 102));

	}

}
