package br.ucsal.bes.poo20181.academia.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.text.ParseException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import br.ucsal.bes.poo20181.academia.controller.LoginController;
import br.ucsal.bes.poo20181.academia.domain.Cliente;
import br.ucsal.bes.poo20181.academia.domain.Pessoa;
import br.ucsal.bes.poo20181.academia.domain.Professor;
import br.ucsal.bes.poo20181.academia.gui.adm.TelaAdm;
import br.ucsal.bes.poo20181.academia.gui.cliente.TelaCliente;
import br.ucsal.bes.poo20181.academia.gui.instrutor.TelaInstrutor;

public class TelaLogin {

	private JFrame janelaLogin;
	private JPanel painel, painel2;
	private JLabel login, senha, text1, text2, imagem, logo;
	private JTextField campoLogin;
	private JPasswordField campoSenha;
	private int xx, xy;

	public static void main(String[] args) {
		TelaLogin tela = new TelaLogin();
		tela.janelaLogin.setVisible(true);
	}

	public TelaLogin() {
		prepararJanela();
		prepararPainel();
		prepararPainelVisual();
		prepararTextPainel();
		adicionarCamposLogin();
		prepararBotaoLogin();
		prepararBotaoFechar();
		prepararBtnMinimizar();
		mostrarJanela();
	}

	private void prepararJanela() {
		janelaLogin = new JFrame("Tela de login");
	}

	private void prepararPainel() {
		painel = new JPanel();
		painel.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				xx = e.getX();
				xy = e.getY();
			}
		});
		painel.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent j) {
				int x = j.getXOnScreen();
				int y = j.getYOnScreen();
				janelaLogin.setLocation(x - xx, y - xy);
			}
		});
		painel.setBackground(Color.WHITE);
		painel.setBorder(new EmptyBorder(5, 5, 5, 5));
		janelaLogin.setContentPane(painel);
		painel.setLayout(null);

	}

	private void prepararPainelVisual() {
		painel2 = new JPanel();
		painel2.setBackground(Color.DARK_GRAY);
		painel2.setBounds(0, -14, 406, 483);
		painel.add(painel2);
		painel2.setLayout(null);

		imagem = new JLabel("");
		imagem.setIcon(
				new ImageIcon(TelaLogin.class.getResource("/br/ucsal/bes/poo20181/academia/gui/img/academia.jpg")));
		imagem.setBounds(0, 11, 406, 242);
		painel2.add(imagem);

		logo = new JLabel("");
		logo.setIcon(new ImageIcon(TelaLogin.class.getResource("/br/ucsal/bes/poo20181/academia/gui/img/logo3.png")));
		logo.setBounds(146, 281, 97, 90);
		painel2.add(logo);

	}

	private void prepararTextPainel() {

		text1 = new JLabel("\" Voc� � a unica pessoa ");
		text1.setForeground(Color.WHITE);
		text1.setHorizontalAlignment(SwingConstants.TRAILING);
		text1.setVerticalAlignment(SwingConstants.TOP);
		text1.setToolTipText("");
		text1.setFont(new Font("Arial Black", Font.PLAIN, 20));
		text1.setBounds(60, 372, 268, 29);
		painel2.add(text1);

		text2 = new JLabel("responsavel pelos seus resultados \".");
		text2.setForeground(Color.WHITE);
		text2.setVerticalAlignment(SwingConstants.TOP);
		text2.setToolTipText("");
		text2.setHorizontalAlignment(SwingConstants.TRAILING);
		text2.setFont(new Font("Arial Black", Font.PLAIN, 18));
		text2.setBounds(10, 414, 369, 29);
		painel2.add(text2);

	}

	private void adicionarCamposLogin() {
		login = new JLabel("LOGIN: ");
		login.setFont(new Font("Nirmala UI", Font.BOLD, 12));
		login.setBounds(487, 99, 65, 14);
		painel.add(login);

		campoLogin = new JTextField();
		campoLogin.setBounds(487, 134, 243, 32);
		painel.add(campoLogin);

		senha = new JLabel("SENHA: ");
		senha.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {  
				      login(null);
			       } 
			}
		});
		senha.setFont(new Font("Nirmala UI", Font.BOLD, 12));
		senha.setBounds(487, 211, 65, 14);
		painel.add(senha);

		campoSenha = new JPasswordField(10);
		campoSenha.setBounds(487, 247, 243, 32);
		painel.add(campoSenha);

	}

	private void prepararBotaoLogin() {
		JButton botaoLogin = new JButton("Login");
		botaoLogin.setFont(new Font("Dialog", Font.BOLD, 12));
		botaoLogin.setForeground(Color.WHITE);
		
				
		botaoLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				login(botaoLogin);
				
			}
		});
		botaoLogin.setBackground(new Color(102, 204, 255));
		botaoLogin.setBounds(487, 360, 243, 40);
		painel.add(botaoLogin);

	}
	
	                                  
	       

	private void prepararBotaoFechar() {
		JLabel fechar = new JLabel("");
		fechar.setIcon(
				new ImageIcon(TelaAdm.class.getResource("/br/ucsal/bes/poo20181/academia/gui/img/Close Window.png")));
		fechar.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent arg0) {
				System.exit(0);
			}
		});
		fechar.setForeground(new Color(102, 204, 255));
		fechar.setFont(new Font("Tahoma", Font.BOLD, 15));
		fechar.setBounds(774, 0, 25, 22);
		painel.add(fechar);

	}

	private void prepararBtnMinimizar() {
		JLabel minimi = new JLabel("");
		minimi.setBounds(745, 0, 25, 22);
		minimi.setIcon(
				new ImageIcon(TelaCliente.class.getResource("/br/ucsal/bes/poo20181/academia/gui/img/Minimize.png")));
		minimi.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getSource().equals(minimi)) {
					janelaLogin.setState(Frame.ICONIFIED);
				}
			}
		});
		minimi.setForeground(new Color(102, 204, 255));
		minimi.setFont(new Font("Tahoma", Font.BOLD, 15));
		painel.add(minimi);

	}

	private void mostrarJanela() {
		janelaLogin.setUndecorated(true);
		janelaLogin.setBounds(100, 100, 799, 465);
		janelaLogin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		janelaLogin.getContentPane().setLayout(null);
	}
	
	
	private void login(JButton botaoLogin) {
		
		Pessoa perfil = LoginController.validarSenha(campoLogin.getText(),
				new String(campoSenha.getPassword()));

		if (perfil != null) {
			switch (perfil.getPerfil()) {
			case PROFESSOR:
				TelaInstrutor telaInstrutor = new TelaInstrutor((Professor)perfil);
				telaInstrutor.janelaInstru.setVisible(true);
				break;
			case ALUNO:
				TelaCliente telaCliente = new TelaCliente((Cliente) perfil);
				telaCliente.janelaCliente.setVisible(true);
				break;
			case ADM:
				TelaAdm tela = null;
				try {
					tela = new TelaAdm(perfil);
				} catch (ParseException e1) {
					
					e1.printStackTrace();
				}
				tela.janelaAdm.setVisible(true);
				break;
			}
			janelaLogin.setVisible(false);
		} else {
			JOptionPane.showMessageDialog(botaoLogin, " Usuario ou senha incorreto. Tente novamente!", "Erro",
					0, null);
		}
		
	}

}
