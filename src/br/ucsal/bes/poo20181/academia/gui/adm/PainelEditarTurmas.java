package br.ucsal.bes.poo20181.academia.gui.adm;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import br.ucsal.bes.poo20181.academia.domain.Pessoa;
import br.ucsal.bes.poo20181.academia.util.ModalidadeEnum;

public class PainelEditarTurmas extends JPanel {

		private static final long serialVersionUID = 1L;
		private JButton modificar, cancelar;

		public PainelEditarTurmas() {
			setBackground(new Color(255, 255, 255));
			setLayout(null);
			setPreferredSize(new Dimension(1000, 465));
			prepararmodificarTurma();
		}

		private void prepararmodificarTurma() {

			JPanel painelSuperior = new JPanel();
			painelSuperior.setBackground(new Color(51, 204, 255));
			painelSuperior.setBounds(0, 0, 574, 56);
			add(painelSuperior);
			painelSuperior.setLayout(null);

			JLabel modificarTurma = new JLabel("Modificar  Turma");
			modificarTurma.setBounds(10, 11, 178, 34);
			modificarTurma.setForeground(Color.WHITE);
			modificarTurma.setFont(new Font("Dialog", Font.BOLD, 16));
			painelSuperior.add(modificarTurma);

			adicionarLabels();
			adicionarComboBox();
			prepararBotoes();
		}

		private void prepararBotoes() {
			adicionarBotaomodificar();
			adicionarBotaocancelar();
		}

		private void adicionarLabels() {
			JLabel txtTurma = new JLabel("Nome da Turma :  ");
			txtTurma.setFont(new Font("Dialog", Font.BOLD, 12));
			txtTurma.setBounds(10, 104, 150, 14);
			add(txtTurma);

			JLabel txtModalidade = new JLabel("Modalidade:");
			txtModalidade.setFont(new Font("Dialog", Font.BOLD, 12));
			txtModalidade.setBounds(10, 160, 90, 14);
			add(txtModalidade);

			JLabel txtInstrutor = new JLabel(" Instrutor : ");
			txtInstrutor.setFont(new Font("Dialog", Font.BOLD, 12));
			txtInstrutor.setBounds(10, 237, 62, 14);
			add(txtInstrutor);

		}

		

		private void adicionarComboBox() {

			JComboBox comboBoxTurmas = new JComboBox();
			comboBoxTurmas.setBounds(20, 129, 442, 20);
			add(comboBoxTurmas);
			
			JComboBox<ModalidadeEnum> comboBoxModalidade = new JComboBox<>(ModalidadeEnum.values());
			comboBoxModalidade.setBounds(20, 195, 231, 20);
			add(comboBoxModalidade);

			JComboBox<Pessoa> instrutores = new JComboBox<Pessoa>();
			instrutores.setBounds(20, 262, 231, 20);
			add(instrutores);

		}

		private void adicionarBotaomodificar() {
			modificar = new JButton("Modificar");
			modificar.setFont(new Font("Dialog", Font.BOLD, 12));
			modificar.setForeground(Color.WHITE);
			modificar.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {

				}
			});
			modificar.setBackground(new Color(102, 204, 255));
			modificar.setBounds(330, 366, 100, 23);
			add(modificar);

		}

		private void adicionarBotaocancelar() {
			cancelar = new JButton("Cancelar");
			cancelar.setFont(new Font("Dialog", Font.BOLD, 12));
			cancelar.setForeground(Color.WHITE);
			cancelar.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					JOptionPane.showConfirmDialog(null, "Realmente deseja cancelar", "Cancelar",
							JOptionPane.YES_NO_OPTION);

									}
			});
			cancelar.setBackground(new Color(102, 204, 255));
			cancelar.setBounds(227, 366, 89, 23);
			add(cancelar);

		}

		

}
