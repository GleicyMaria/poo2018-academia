package br.ucsal.bes.poo20181.academia.gui.adm;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.MaskFormatter;

import br.ucsal.bes.poo20181.academia.controller.ProfessorController;
import br.ucsal.bes.poo20181.academia.gui.principais.PainelCadastro;
import br.ucsal.bes.poo20181.academia.util.ModalidadeEnum;
import br.ucsal.bes.poo20181.academia.util.SexoEnum;

public class PainelCadastrarProfessor extends PainelCadastro {

	protected static final long serialVersionUID = 1L;

	private JLabel dadosProfissionais, formacao;
	private JTextField campoFormacao;

	public PainelCadastrarProfessor() throws ParseException {
		super();
		
	}

	@Override
	protected void prepararCadasto() throws ParseException {

		painelSuperior = new JPanel();
		painelSuperior.setBackground(new Color(51, 204, 255));
		painelSuperior.setBounds(0, 0, 574, 56);
		add(painelSuperior);
		painelSuperior.setLayout(null);

		cadastrar =  new JLabel("Cadastrar Professor");
		cadastrar.setBounds(10, 11, 178, 34);
		cadastrar.setForeground(Color.WHITE);
		cadastrar.setFont(new Font("Dialog", Font.BOLD, 16));
		painelSuperior.add(cadastrar);
		
		prepararLabels();
		prepararCampos();
		prepararBotoes();

	}

	@Override
	protected void adicionarLabelsAtividade() {
		dadosProfissionais = new JLabel("DADOS PROFISSIONAIS:");
		dadosProfissionais.setFont(new Font("Dialog", Font.BOLD, 12));
		dadosProfissionais.setBounds(10, 270, 148, 14);
		add(dadosProfissionais);

		formacao = new JLabel("Forma��o:");
		formacao.setFont(new Font("Dialog", Font.BOLD, 12));
		formacao.setBounds(10, 300, 61, 22);
		add(formacao);

		modalidade = new JLabel("Modalidade:");
		modalidade.setFont(new Font("Dialog", Font.BOLD, 12));
		modalidade.setBounds(260, 300, 83, 14);
		add(modalidade);
	}

	@Override
	protected void adicionarCamposAtividade() {
		campoFormacao = new JTextField();
		campoFormacao.setColumns(10);
		campoFormacao.setBounds(72, 300, 165, 20);
		add(campoFormacao);

		JComboBox<ModalidadeEnum> comboBoxModalidade = new JComboBox<>(ModalidadeEnum.values());
		comboBoxModalidade.setBounds(333, 300, 210, 20);
		add(comboBoxModalidade);
	}

}
