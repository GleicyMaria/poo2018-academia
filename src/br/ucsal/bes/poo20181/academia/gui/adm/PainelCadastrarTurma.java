package br.ucsal.bes.poo20181.academia.gui.adm;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import br.ucsal.bes.poo20181.academia.domain.Pessoa;
import br.ucsal.bes.poo20181.academia.util.ModalidadeEnum;

public class PainelCadastrarTurma extends JPanel {

	private static final long serialVersionUID = 1L;
	private JTextField campoNomeTurma;
	private JButton cadastrar, limpar;

	public PainelCadastrarTurma() {
		setBackground(new Color(255, 255, 255));
		setLayout(null);
		setPreferredSize(new Dimension(1000, 465));
		prepararcadastrarTurma();
	}

	private void prepararcadastrarTurma() {

		JPanel painelSuperior = new JPanel();
		painelSuperior.setBackground(new Color(51, 204, 255));
		painelSuperior.setBounds(0, 0, 574, 56);
		add(painelSuperior);
		painelSuperior.setLayout(null);

		JLabel cadastrarTurma = new JLabel("Cadastrar Nova Turma");
		cadastrarTurma.setBounds(10, 11, 178, 34);
		cadastrarTurma.setForeground(Color.WHITE);
		cadastrarTurma.setFont(new Font("Dialog", Font.BOLD, 16));
		painelSuperior.add(cadastrarTurma);

		adicionarLabels();
		adicionarCampo();
		adicionarComboBox();
		prepararBotoes();
	}

	private void prepararBotoes() {
		adicionarBotaoCadastrar();
		adicionarBotaoLimpar();
	}

	private void adicionarLabels() {
		JLabel txtTurma = new JLabel("Nome da Turma :  ");
		txtTurma.setFont(new Font("Dialog", Font.BOLD, 12));
		txtTurma.setBounds(10, 104, 150, 14);
		add(txtTurma);

		JLabel txtModalidade = new JLabel("Modalidade:");
		txtModalidade.setFont(new Font("Dialog", Font.BOLD, 12));
		txtModalidade.setBounds(10, 160, 90, 14);
		add(txtModalidade);

		JLabel txtInstrutor = new JLabel(" Instrutor : ");
		txtInstrutor.setFont(new Font("Dialog", Font.BOLD, 12));
		txtInstrutor.setBounds(10, 237, 62, 14);
		add(txtInstrutor);

	}

	private void adicionarCampo() {
		campoNomeTurma = new JTextField();
		campoNomeTurma.setBounds(20, 129, 442, 20);
		add(campoNomeTurma);
		campoNomeTurma.setColumns(10);

	}

	private void adicionarComboBox() {

		JComboBox<ModalidadeEnum> comboBoxModalidade = new JComboBox<>(ModalidadeEnum.values());
		comboBoxModalidade.setBounds(20, 195, 231, 20);
		add(comboBoxModalidade);

		JComboBox<Pessoa> instrutores = new JComboBox<Pessoa>();
		instrutores.setBounds(20, 262, 231, 20);
		add(instrutores);

	}

	private void adicionarBotaoCadastrar() {
		cadastrar = new JButton("Cadastrar");
		cadastrar.setFont(new Font("Dialog", Font.BOLD, 12));
		cadastrar.setForeground(Color.WHITE);
		cadastrar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

			}
		});
		cadastrar.setBackground(new Color(102, 204, 255));
		cadastrar.setBounds(330, 366, 100, 23);
		add(cadastrar);

	}

	private void adicionarBotaoLimpar() {
		limpar = new JButton("Limpar");
		limpar.setFont(new Font("Dialog", Font.BOLD, 12));
		limpar.setForeground(Color.WHITE);
		limpar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int resp = JOptionPane.showConfirmDialog(null, "Realmente deseja limpar o formulario", "Limpa",
						JOptionPane.YES_NO_OPTION);

				if (resp == JOptionPane.YES_OPTION) {
					limparCampos();
				}
			}
		});
		limpar.setBackground(new Color(102, 204, 255));
		limpar.setBounds(227, 366, 89, 23);
		add(limpar);

	}

	private void limparCampos() {
		campoNomeTurma.setText(null);
	}
}
