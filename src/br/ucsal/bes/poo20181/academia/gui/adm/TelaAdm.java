package br.ucsal.bes.poo20181.academia.gui.adm;

import java.awt.Color;
import java.awt.Font;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.text.ParseException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import br.ucsal.bes.poo20181.academia.domain.Pessoa;
import br.ucsal.bes.poo20181.academia.gui.TelaLogin;
import br.ucsal.bes.poo20181.academia.gui.cliente.TelaCliente;
import br.ucsal.bes.poo20181.academia.gui.principais.PainelEditarDados;

public class TelaAdm {

	public JFrame janelaAdm;
    private JPanel painelPrincipal, painelLateral;
    private JButton botaoCadasTurma, botaoCadastProfes, botaoCadastClient, botaoAcompanhamento, botaoEditarDados,
			botaoEditarTurmas, logout;
    private JScrollPane scroll;
	private int xx, xy;
    private PainelCadastrarProfessor cadastroProf;
    private PainelCadastrarCliente cadastroCliente;
    private PainelCadastrarTurma cadastroTurma;
    private PainelEditarDados editarDados;
    private PainelEditarTurmas editarTurma;
	private Pessoa usuario;

	public static void main(String[] args) throws ParseException {
		TelaAdm tela = new TelaAdm(new Pessoa());
		tela.janelaAdm.setVisible(true);

	}

	public TelaAdm(Pessoa usuario) throws ParseException {
        this.usuario = usuario;
		prepararJanela();
		prepararPainelPrincipal();
		iniciarPaineis();
		prepararPainelLateral();
		prepararBtnCadasTurma();
		prepararBtnCadastProf();
		prepararBtnCadastClient();
		prepararBtnAcompanhamento();
		prepararBtnEditarDados();
		prepararBtnEditarTurmas();
		prepararBtnFechar();
		prepararBtnMinimizar();
		prepararIdentificador();
		prepararBtnLogout();

	}

	private void prepararJanela() {
		janelaAdm = new JFrame();
		janelaAdm.setUndecorated(true);
		janelaAdm.setResizable(false);
		janelaAdm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		janelaAdm.setBounds(100, 100, 799, 465);

	}

	private void prepararPainelPrincipal() {
		painelPrincipal = new JPanel();
		painelPrincipal.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {

				xx = e.getX();
				xy = e.getY();
			}
		});
		painelPrincipal.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent arg0) {
				int x = arg0.getXOnScreen();
				int y = arg0.getYOnScreen();

				janelaAdm.setLocation(x - xx, y - xy);

			}
		});
		painelPrincipal.setBackground(new Color (255,255,255));
		painelPrincipal.setBorder(new EmptyBorder(5, 5, 5, 5));
		janelaAdm.setContentPane(painelPrincipal);
		painelPrincipal.setLayout(null);

	}

	private void prepararPainelLateral() {
		painelLateral = new JPanel();
		painelLateral.setBounds(0, 0, 225, 465);
		painelLateral.setForeground(new Color(0, 0, 0));
		painelLateral.setBackground(new Color(102, 102, 102));
		painelPrincipal.add(painelLateral);
		painelLateral.setLayout(null);

	}

	private void iniciarPaineis() throws ParseException {
		cadastroProf = new PainelCadastrarProfessor();
		cadastroCliente = new PainelCadastrarCliente();
		cadastroTurma = new PainelCadastrarTurma();
		editarDados = new PainelEditarDados(usuario);
		editarTurma = new PainelEditarTurmas();
		
		scroll = new JScrollPane();
		scroll.setBounds(225, 23, 574, 465);

		painelPrincipal.add(scroll);

	}

	private void prepararBtnCadasTurma() {

		botaoCadasTurma = new JButton();
		botaoCadasTurma .addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource().equals(botaoCadasTurma)) {
					scroll.setViewportView(cadastroTurma);
				}
			}

		});

		botaoCadasTurma.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				setColor(botaoCadasTurma);
				resetColor(botaoCadastProfes);
				resetColor(botaoCadastClient);
				resetColor(botaoAcompanhamento);
				resetColor(botaoEditarDados);
				resetColor(botaoEditarTurmas);
				resetColor(logout);

			}
		});
		botaoCadasTurma.setBackground(new Color(102, 102, 102));
		botaoCadasTurma.setBounds(0, 96, 225, 35);
		painelLateral.add(botaoCadasTurma);
		botaoCadasTurma.setLayout(null);

		JLabel textBtnCadasTurma = new JLabel("Cadastro Turma");
		textBtnCadasTurma.setForeground(new Color(255, 255, 255));
		textBtnCadasTurma.setFont(new Font("Dialog", Font.BOLD, 16));
		textBtnCadasTurma.setBounds(45, 11, 150, 14);
		botaoCadasTurma.add(textBtnCadasTurma);

		JLabel iconeCadasTurma = new JLabel("");
		iconeCadasTurma
				.setIcon(new ImageIcon(TelaAdm.class.getResource("/br/ucsal/bes/poo20181/academia/gui/img/grupo.png")));
		iconeCadasTurma.setBounds(10, 8, 25, 22);
		botaoCadasTurma.add(iconeCadasTurma);
	}

	private void prepararBtnCadastProf() {

		botaoCadastProfes = new JButton();
		botaoCadastProfes.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource().equals(botaoCadastProfes)) {
					scroll.setViewportView(cadastroProf);
				}
			}

		});

		botaoCadastProfes.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {

				setColor(botaoCadastProfes);
				resetColor(botaoCadasTurma);
				resetColor(botaoCadastClient);
				resetColor(botaoAcompanhamento);
				resetColor(botaoEditarDados);
				resetColor(botaoEditarTurmas);
				resetColor(logout);

			}
		});

		botaoCadastProfes.setLayout(null);
		botaoCadastProfes.setBackground(new Color(102, 102, 102));
		botaoCadastProfes.setBounds(0, 132, 225, 35);
		painelLateral.add(botaoCadastProfes);

		JLabel txtBtnCadastroProfessor = new JLabel("Cadastro Professor");
		txtBtnCadastroProfessor.setForeground(Color.WHITE);
		txtBtnCadastroProfessor.setFont(new Font("Dialog", Font.BOLD, 16));
		txtBtnCadastroProfessor.setBounds(45, 11, 150, 14);
		botaoCadastProfes.add(txtBtnCadastroProfessor);

		JLabel iconeCadastroProfe = new JLabel("");
		iconeCadastroProfe
				.setIcon(new ImageIcon(TelaAdm.class.getResource("/br/ucsal/bes/poo20181/academia/gui/img/Coach.png")));
		iconeCadastroProfe.setBounds(10, 8, 25, 22);
		botaoCadastProfes.add(iconeCadastroProfe);

	}

	private void prepararBtnCadastClient() {
		botaoCadastClient = new JButton();
		
		botaoCadastClient.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource().equals(botaoCadastClient)) {
					scroll.setViewportView(cadastroCliente);
				}
			}

		});
		

		botaoCadastClient.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {

				setColor(botaoCadastClient);
				resetColor(botaoCadasTurma);
				resetColor(botaoCadastProfes);
				resetColor(botaoAcompanhamento);
				resetColor(botaoEditarDados);
				resetColor(botaoEditarTurmas);
				resetColor(logout);
			}
		});
		botaoCadastClient.setLayout(null);
		botaoCadastClient.setBackground(new Color(102, 102, 102));
		botaoCadastClient.setBounds(0, 168, 225, 35);
		painelLateral.add(botaoCadastClient);

		JLabel iconeCadastCliente = new JLabel("");
		iconeCadastCliente.setIcon(
				new ImageIcon(TelaAdm.class.getResource("/br/ucsal/bes/poo20181/academia/gui/img/Addcliente.png")));
		iconeCadastCliente.setBounds(10, 8, 25, 22);
		botaoCadastClient.add(iconeCadastCliente);

		JLabel textBtnCadastClient = new JLabel("Cadastro Cliente");
		textBtnCadastClient.setForeground(Color.WHITE);
		textBtnCadastClient.setFont(new Font("Dialog", Font.BOLD, 16));
		textBtnCadastClient.setBounds(45, 11, 150, 14);
		botaoCadastClient.add(textBtnCadastClient);

	}

	private void prepararBtnAcompanhamento() {
		botaoAcompanhamento = new JButton();

		botaoAcompanhamento.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {

				setColor(botaoAcompanhamento);
				resetColor(botaoCadasTurma);
				resetColor(botaoCadastClient);
				resetColor(botaoCadastProfes);
				resetColor(botaoEditarDados);
				resetColor(botaoEditarTurmas);
				resetColor(logout);
			}
		});
		botaoAcompanhamento.setLayout(null);
		botaoAcompanhamento.setBackground(new Color(102, 102, 102));
		botaoAcompanhamento.setBounds(0, 204, 225, 35);
		painelLateral.add(botaoAcompanhamento);

		JLabel txtBtnAcompa = new JLabel("Acompanhamento");
		txtBtnAcompa.setForeground(Color.WHITE);
		txtBtnAcompa.setFont(new Font("Dialog", Font.BOLD, 16));
		txtBtnAcompa.setBounds(45, 4, 150, 24);
		botaoAcompanhamento.add(txtBtnAcompa);

		JLabel iconeAcompanha = new JLabel("");
		iconeAcompanha.setIcon(
				new ImageIcon(TelaAdm.class.getResource("/br/ucsal/bes/poo20181/academia/gui/img/Inspection.png")));
		iconeAcompanha.setBounds(10, 8, 25, 22);
		botaoAcompanhamento.add(iconeAcompanha);

	}

	private void prepararBtnEditarDados() {
		botaoEditarDados = new JButton();
		botaoEditarDados .addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource().equals(botaoEditarDados)) {
					scroll.setViewportView(editarDados);
				}
			}

		});

		botaoEditarDados.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {

				setColor(botaoEditarDados);
				resetColor(botaoEditarTurmas);
				resetColor(botaoCadasTurma);
				resetColor(botaoCadastClient);
				resetColor(botaoAcompanhamento);
				resetColor(botaoCadastProfes);
				resetColor(logout);
			}
		});
		botaoEditarDados.setLayout(null);
		botaoEditarDados.setBackground(new Color(102, 102, 102));
		botaoEditarDados.setBounds(0, 240, 225, 35);
		painelLateral.add(botaoEditarDados);

		JLabel txtBtnEditarDados = new JLabel("Editar dados ");
		txtBtnEditarDados.setForeground(Color.WHITE);
		txtBtnEditarDados.setFont(new Font("Dialog", Font.BOLD, 16));
		txtBtnEditarDados.setBounds(45, 11, 150, 14);
		botaoEditarDados.add(txtBtnEditarDados);

		JLabel iconeEditarDados = new JLabel("");
		iconeEditarDados.setIcon(
				new ImageIcon(TelaAdm.class.getResource("/br/ucsal/bes/poo20181/academia/gui/img/Create.png")));
		iconeEditarDados.setBounds(10, 8, 25, 22);
		botaoEditarDados.add(iconeEditarDados);

	}

	private void prepararBtnEditarTurmas() {
		botaoEditarTurmas = new JButton();
		
		botaoEditarTurmas .addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource().equals(botaoEditarTurmas)) {
					scroll.setViewportView(editarTurma);
				}
			}

		});

		botaoEditarTurmas.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {

				setColor(botaoEditarTurmas);
				resetColor(botaoCadasTurma);
				resetColor(botaoCadastClient);
				resetColor(botaoAcompanhamento);
				resetColor(botaoCadastProfes);
				resetColor(botaoEditarDados);
				resetColor(logout);
			}
		});
		botaoEditarTurmas.setLayout(null);
		botaoEditarTurmas.setBackground(new Color(102, 102, 102));
		botaoEditarTurmas.setBounds(0, 276, 225, 35);
		painelLateral.add(botaoEditarTurmas);

		JLabel txtBtnEditarTurmas= new JLabel("Editar turmas ");
		txtBtnEditarTurmas.setForeground(Color.WHITE);
		txtBtnEditarTurmas.setFont(new Font("Dialog", Font.BOLD, 16));
		txtBtnEditarTurmas.setBounds(45, 11, 150, 14);
		botaoEditarTurmas.add(txtBtnEditarTurmas);

		JLabel iconeEditarTurmas = new JLabel("");
		iconeEditarTurmas.setIcon(
				new ImageIcon(TelaAdm.class.getResource("/br/ucsal/bes/poo20181/academia/gui/img/grup.png")));
		iconeEditarTurmas.setBounds(10, 8, 25, 22);
		botaoEditarTurmas.add(iconeEditarTurmas);

	}

	private void prepararIdentificador() {
		JLabel identificador = new JLabel("Adm : " + usuario.getNome());
		identificador.setForeground(new Color(255, 255, 255));
		identificador.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 16));
		identificador.setBounds(10, 399, 250, 22);
		painelLateral.add(identificador);

	}

	private void prepararBtnLogout() {
		logout = new JButton();
		logout.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {

				setColor(logout);
				resetColor(botaoAcompanhamento);
				resetColor(botaoCadastClient);
				resetColor(botaoCadasTurma);
				resetColor(botaoEditarDados);
				resetColor(botaoCadastProfes);
				resetColor(botaoEditarTurmas);

				int resp = JOptionPane.showConfirmDialog(null, "Deseja realmente fazer logout?", "Logout",
						JOptionPane.YES_NO_OPTION);

				if (resp == JOptionPane.YES_OPTION) {
					janelaAdm.setVisible(false);
					TelaLogin.main(null);
				}

			}
		});
		logout.setBackground(new Color(102, 102, 102));
		logout.setBounds(10, 432, 45, 22);
		painelLateral.add(logout);
		logout.setLayout(null);

		JLabel txtBtnLogout = new JLabel("Logout");
		txtBtnLogout.setBounds(0, 0, 39, 15);
		txtBtnLogout.setForeground(new Color(255, 255, 255));
		txtBtnLogout.setFont(new Font("Dialog", Font.BOLD, 11));
		logout.add(txtBtnLogout);
	}

	private void prepararBtnMinimizar() {
		JLabel minimi = new JLabel("");
		minimi.setBounds(745, 0, 25, 22);
		minimi.setIcon(
				new ImageIcon(TelaCliente.class.getResource("/br/ucsal/bes/poo20181/academia/gui/img/Minimize.png")));
		minimi.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getSource().equals(minimi)) {
					janelaAdm.setState(Frame.ICONIFIED);
				}
			}
		});
		minimi.setForeground(new Color(102, 204, 255));
		minimi.setFont(new Font("Tahoma", Font.BOLD, 15));
		painelPrincipal.add(minimi);

	}

	private void prepararBtnFechar() {
		JLabel fechar = new JLabel("");
		fechar.setBounds(774, 0, 25, 22);
		fechar.setIcon(
				new ImageIcon(TelaAdm.class.getResource("/br/ucsal/bes/poo20181/academia/gui/img/Close Window.png")));
		fechar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				System.exit(0);
			}
		});
		fechar.setForeground(new Color(102, 204, 255));
		fechar.setFont(new Font("Tahoma", Font.BOLD, 15));
		painelPrincipal.add(fechar);

	}

	private void setColor(JButton button) {

		button.setBackground(new Color(153, 153, 153));

	}

	private void resetColor(JButton button) {

		button.setBackground(new Color(102, 102, 102));

	}

}
