package br.ucsal.bes.poo20181.academia.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes.poo20181.academia.domain.Cliente;
import br.ucsal.bes.poo20181.academia.domain.Endereco;
import br.ucsal.bes.poo20181.academia.domain.Pessoa;
import br.ucsal.bes.poo20181.academia.domain.Professor;
import br.ucsal.bes.poo20181.academia.domain.Turma;
import br.ucsal.bes.poo20181.academia.util.ModalidadeEnum;
import br.ucsal.bes.poo20181.academia.util.PerfilEnum;

public class Academia {
	public static List<Pessoa> usuarios;
	public static List<Turma> turmas;
	private static  SimpleDateFormat data= new SimpleDateFormat ("dd/MM/yyyy");

	static {
		usuarios = new ArrayList<>();
		turmas = new ArrayList<>();

		try {
			usuarios.add(new Pessoa("admin", data.parse("11/02/1998"), new Endereco("Cabula VI", "Rua A", 14, "Salvador", "41587965")));
			usuarios.get(0).setPerfil(PerfilEnum.ADM);
			
		   // Popular lista de professor
		   usuarios.add(new Professor("Gleicy Maria", data.parse("02/11/1996"), new Endereco("Sitio Novo", "Rua B", 19, "Candeias", "41579585"),
								"Ed. Fisica", ModalidadeEnum.NATACAO));
		   usuarios.get(1).setCpf("89764315245"); 
		   usuarios.add(new Professor("Ryu",data.parse("11/01/1985"), new Endereco("Cabula VI", "Rua A", 14, "Salvador", "418796565"), "Ed. Fisica",
								ModalidadeEnum.DANCA));
		
		   usuarios.add(new Professor("Ken Masters", data.parse("01/10/1990"), new Endereco("Cajazeiras", "Rua P", 15, "Salvador", "25877965"),
					"Ed. Fisica", ModalidadeEnum.MUSCULACAO));
			usuarios.add(new Professor("Guile", data.parse("28/02/1994"), new Endereco("Lapinha", "Rua O", 1, "Salvador", "41587465"), "Ed. Fisica",
					ModalidadeEnum.NATACAO));
			usuarios.add(new Professor("Sagat", data.parse("08/09/1986"), new Endereco("S�o Marcos", "Rua G", 19, "Salvador", "25478964"),
					"Ed. Fisica", ModalidadeEnum.DANCA));
			usuarios.add(new Professor("Chun-Li", data.parse("17/06/1998"), new Endereco("Pituba", "Rua Z", 12, "Salvador", "2547896314"), "Ed. Fisica",
					ModalidadeEnum.MUSCULACAO));

			// Popular lista de alunos
			usuarios.add(new Cliente("Jean Xavier", data.parse("26/08/1995"), new Endereco("Cabula VI", "Rua A", 14, "Salvador", "147852369")));
			usuarios.add(new Cliente("Pedro Herinque", data.parse("13/05/1993"), new Endereco("Lapinha", "Rua Z", 24, "Salvador","547896123")));
			usuarios.add(new Cliente("Marcos Santana",data.parse("04/07/1997"), new Endereco("Patamares", "Rua G", 14, "Salvador","764589312")));

			// Popular lista de turmas
	        List<Cliente> alunos = getAlunos();
	        turmas.add(new Turma(usuarios.get(0), alunos));
			turmas.add(new Turma(usuarios.get(1), alunos));
			turmas.add(new Turma(usuarios.get(2), alunos));
			turmas.add(new Turma(usuarios.get(3), alunos));
			turmas.add(new Turma(usuarios.get(4), alunos));
		   
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        

		
		
		
		
	
	}
	
	public static List<Professor> getProfessores() {
		List<Professor> professores = new ArrayList<>();
		
		for (Pessoa usuario : usuarios) {
			if(usuario.getPerfil() == PerfilEnum.PROFESSOR) {
				professores.add((Professor) usuario);
			}
		}
		
		return professores;
	}

	public static List<Cliente> getAlunos(){
		List<Cliente> alunos = new ArrayList<>();

		for (Pessoa aluno : usuarios) {
			if(aluno.getPerfil() == PerfilEnum.ALUNO){
				alunos.add((Cliente) aluno);
			}
		}

		return alunos;
	}
}
